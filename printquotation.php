<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Youngman | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<?php

include("secure/db_connect.php");
include("secure/functions.php");
sec_session_start();
$page = basename($_SERVER['PHP_SELF']);


$s_no = $_GET['id'];

$sql = "SELECT q.row_total, q.sub_total, q.freight, q.tax, q.total, q.createdby, q.customer_id, q.customer_name, q.delivery_address, q.delivery_date, q.duration_billing, q.billing_address_line_1, q.billing_address_line_2,q.billing_city , q.billing_pincode, l.mailing_address, l.mailing_pincode, q.contact_name, q.security_amt FROM table_quotation AS q, qb_cache_customer AS c, customer_local AS l WHERE q.s_no = ? AND c.customer_id = q.customer_id AND c.customer_id = l.customer_id";
if($info = $mysqli->prepare($sql)){
$info = $mysqli->prepare($sql);
$info->bind_param('s', $s_no);
$info->execute();
$info->store_result();
$info->bind_result( $row_total, $sub_total, $freight, $tax, $total, $created_by, $customer_id, $customer_name, $delivery_address, $delivery_date, $duration_billing, $billing_address_line_1, $billing_address_line_2, $billing_city, $billing_pincode, $mailing_address, $mailing_pincode, $contact_name, $security_amt);
$info->fetch();
}else echo "prepare failed".$mysqli->error;

$string = ' ';
    
    $billing_address = $billing_address_line_1."<br>".$billing_address_line_2."<br>".$billing_city."<br>".$billing_pincode;

$stmt =  $mysqli->prepare("SELECT `type`, `item_code`, `desc`, `unit_price`, `qty`, `tot`  FROM table_quotation_item WHERE s_no = ?");

if($stmt){
  //  echo "prepared";
      $stmt->bind_param('s',  $s_no); 
    $stmt->execute(); // Execute the prepared query.
    $stmt->store_result();
    $stmt->bind_result($type, $item_code, $desc, $unit_price, $qty, $tot); // get variables from result
     $si = 0;
    while($stmt->fetch()) {
        $si++;
     $string.="<tr>
     <td>$si</td>
     <td>$item_code</td>
     <td>$desc</td>
     <td>$unit_price/Month</td>
     <td>$qty</td>
     <td>$tot</td>
     </tr>";
    }
    
}else echo "prepare failed".$mysqli->error;


?>
<body onload="window.print();">
    <!--  Wrapper. Contains page content -->
    <div class="wrapper">
        <!-- Main content -->
          <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img src="logo.png" alt="Youngman India Pvt. Ltd.">
                        <medium class="pull-right">Rental Quotation</medium>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div>
                <strong>Customer ID:</strong>
                <?php echo $customer_id; ?><br>
                <strong>Customer Name:</strong>
                <?php echo $customer_name; ?>

            </div>
            <hr>

            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">

                    <strong> Kind Attention</strong>
                    <address>
             <?php echo $contact_name."<br>".$mailing_address."<br>".$mailing_pincode;?>
          </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <strong>  Delivery Address</strong>
                    <address>
            <?php echo $delivery_address;?>
          </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <strong>  Billing Address</strong>
                    <address>
            <?php echo $billing_address;?>
          </address>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Item Code</th>
                                <th>Description</th>

                                <th>Unit Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $string; ?>
                            <tr>
                                <th>Total</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>₹
                                    <?php echo $row_total;?>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->

                <div class="col-xs-6 text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    <p>
                        <b style="margin-left: 10px;">Terms and Conditions</b>
                         <ol>
                            <li>Payment: 100% alongwith the work order.</li>
                            <li>Any other govt levy, or local govt entry charge shall be charged extra if any</li>
                            <li>YOUNGMAN STANDARD HIRE TERMS must be signed, acknowledged and sent to us in order to process the material and commence the rental of the aforesaid.</li>
                            <li>Security cheque of amount : INR <?php echo $security_amt; ?>/- must be provided by you. Kindly note that this cheque can be post dated also. This is only for security purpose and shall not be encashed by YOUNGMAN INDIA PVT. LTD. under normal circumstances.</li>
                            <li>Any damage in the material shall be treated in accordance with the Rental Agreement/YOUNGMAN STD. HIRE TERMS.</li>
                        </ol>
                    </p>
                </div>
                <!-- /.col -->
                <div class="col-xs-6">
                    <!--<p class="lead">Amount Due 2/22/2014</p>-->

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">Freight:</th>
                                <td>₹
                                    <?php echo $freight;?>
                                </td>
                            </tr>
                            <tr>
                                <th>Subtotal:</th>
                                <td>₹
                                    <?php echo $sub_total;?>
                                </td>
                            </tr>
                            <tr>
                                <th>Tax</th>
                                <td>₹
                                    <?php echo $tax;?>
                                </td>
                            </tr>
                        
                            <tr>
                                <th>Total:</th>
                                <td>₹
                                    <?php echo $total;?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    <a href="printquotation.php?id=<?php echo $s_no; ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                </div>
            </div>
        </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
