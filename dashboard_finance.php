<?php include ("includes/header.php");?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
<link href="dist/css/jquery-ui.min.css" rel="stylesheet">
   <script src="dist/js/update_customer.js"></script>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Customer Properties
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Customer</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
   

        <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

             <div class="form-group">
                <label for="id">Customer ID</label>
                <input type="text" class="form-control" name="id" id="id" placeholder="Enter ID" autocomplete="off" required readonly>
              </div>
              <div class="form-group">
                 <label for="name">Name</label>
                 <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name (required)" autocomplete="off" required>
               </div>

              <div id="suggesstion-box"></div>
                                         
                <div class="form-group">
                  <label for="creditLimit" class="col-sm-2 control-label">Credit Limit</label>
                  <div class="col-sm-10 row">
                    <input class="form-control" id="creditLimit" placeholder="Credit Limit" name="creditLimit" type="text" required>
                  </div>
                </div>
                  
              <div class="form-group">
                  <label for="creditTerm" class="col-sm-2 control-label">Credit Term</label>
                  <div class="col-sm-10 row">
                    <input class="form-control" id="creditTerm" placeholder="Credit Term" name="creditTerm" type="text" required>
                  </div>
              </div>
              

                     <div class="form-group">
                  <label for="inputMailAddress" class="col-sm-2 control-label">Mailing Address</label>
                  <div class="col-sm-10 row" id="inputMailAddress">
                    <input class="form-control col-sm-3" id="mailAddLine" name="mailAddLine" placeholder="Mailing Address" type="text" required>
                         <input class="form-control col-sm-3" id="mailAddPin" name="mailAddPin" placeholder="Pincode" type="number" required>
                  </div>
                </div>

             
                   <div class="form-group">
                       <div class="col-sm-offset-2 col-sm-10">
                           
                           <div class="form-group">
                  <label>Security Letter</label>
                  <select class="form-control" name="securityLetter">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                </div>
                           
                           <div class="form-group">
                  <label>Rental Advance</label>
                  <select class="form-control" name="rentalAdvance">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                </div>
                           
                           <div class="form-group">
                  <label>Rental Order</label>
                  <select class="form-control" name="rentalOrder">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                </div>
                           
                           <div class="form-group">
                  <label>Security Cheque</label>
                  <select class="form-control" name="securityCheque">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                </div>
            
         
                  
              </div>
              
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
        
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>

