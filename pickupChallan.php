<?php include ("includes/header.php");?>
<?php 
error_reporting(E_ALL); ini_set('display_errors', 1);

if( $_SESSION['role']==='planning' ){
    
    $challan_id = $_POST['challan_id'];
    $job_order = $_POST['job_order'];

$rem = "SELECT remarks FROM table_challan WHERE challan_id = ?";
                                            if($stmt = $mysqli->prepare($rem)){
                                                $stmt->bind_param('s', $challan_id);
                                                $stmt->execute();
                                                $stmt->store_result();
                                                $stmt->bind_result($remarks );
                                                $stmt->fetch();
                                            }


?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Verify
        </h1>   
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Verify</li>
        </ol>
        <br>

	          <strong>Job Order:</strong> <?php echo $job_order;?><br>
 <strong>Remarks:</strong><?php echo $remarks; ?><br>
        
    </section>

    <!-- Main content -->
    <section class="content">
       
            <!-- Left col -->
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Recieved Material</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class='row'>

                                <table class="table table-striped table-hover" id="table_challan_rental">
                                    <thead>
                                        <tr>
                                          
                                            <th>Item No</th>
                                            <th>Description</th>
                                            <th>Quantity</th>
                                            <th>Missing</th>
                                            <th>Damage</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $recieved_items = "SELECT item_id, item_description, quantity, missing, damage FROM challan_item_relation WHERE challan_id = ?";
                                            if($stmt = $mysqli->prepare($recieved_items)){
                                                $stmt->bind_param('s', $challan_id);
                                                $stmt->execute();
                                                $stmt->store_result();
                                                $stmt->bind_result($item_id, $item_description, $quantity, $missing, $damage );
                                            }
                                            while($stmt->fetch()){
                                                ?>
                                        <tr>
                                            <td><?php echo $item_id; ?></td>
                                            <td><?php echo $item_description; ?></td>
                                            <td><?php echo $quantity; ?></td>
                                            <td><?php echo $missing; ?></td>
                                            <td><?php echo $damage; ?></td>
                                        </tr>
                                        <?php
                                            }
    
                                        ?>
                                    </tbody>
                                </table>

                            </div>

                          
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- ./col -->
                <div class="col-md-4">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Billed Material</h3>
                            
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-striped required_items" id="required_items">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Item ID</th>
                                        <th>Type</th>
                                        <th>Qty</th>
                                        <th>Sub</th>
                                    </tr>
                                    <?php $s_no = 0;
                            
                            $recieved_items = "select c.item_code, t.bundle, SUM( c.qty ) FROM table_billing AS c, table_item AS t WHERE c.job_order = ? AND c.item_code = t.item_code GROUP BY item_code";
                                            if($stmt = $mysqli->prepare($recieved_items)){
                                                $stmt->bind_param('s', $job_order);
                                                $stmt->execute();
                                                $stmt->store_result();
                                                $stmt->bind_result($item_code, $bundle, $qty);
                                            }
                              //  echo "select c.item_code, t.bundle, c.qty FROM table_billing AS c, table_item AS t WHERE c.job_order = '$job_order' AND c.item_code = t.item_code";
                            while($stmt->fetch())  {
                        $s_no++;
                            $bundle ? $bundle="Bundle" : $bundle = "Item"; 
                            echo ' <tr id="'.$item_code.'">
                              <td>'.$s_no.'</td>
                              <td>'.$item_code.'</td>
                              <td>'.$bundle.'</td>
                              <td> '.$qty.'</td>
                              
                              <td><button type="button" class="btnSelect btn-primary">Sub</button></td>
                            </tr>';
                
                }?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Stop Billing</h3>
                        </div>
                        <!-- /.box-header -->
            <form role="form" method="post" action="stop_billing.php" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo $_SESSION['login_string']; ?>" name ="csrf" > 
                        <div class="box-body no-padding">
                            <table class="table table-striped" id="fullfilled_items">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Item ID</th>
                                        <th>Quantity</th>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        
            <input type="hidden" name="challan_id" value="<?php echo $challan_id; ?>" />
           
            <input type="hidden" name="job_order" value="<?php echo $job_order; ?>" />
            <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
        </form>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- ./col -->
            </div>
            <!-- /.col -->


        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php 

}
include ("includes/footer.php"); ?>
<!--<script src="dist/js/gui_planning.js"></script>-->
<script>

    
    var from_location = $("#from_location").text();




    var specialKeys = new Array();
specialKeys.push(8,46); //Backspace
function IsNumeric(e) {
    var keyCode = e.which ? e.which : e.keyCode;
    console.log( keyCode );
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    return ret;
}
    


    


$("#required_items").on('click', '.btnSelect', function () {
    // alert("clicked");

    var currentRow = $(this).closest("tr");
    var ID = currentRow.find("td:eq(1)").text();
    var bundle = currentRow.find("td:eq(2)").text();
    var qty = currentRow.find("td:eq(3)").text();

    // alert(desc);
    qty = parseInt(qty, 10);
    if (qty > 0) {
        currentRow.find("td:eq(3)").text(qty - 1);

        var row_id = $("#fullfilled_items").find('#' + ID).html();
        //   alert(row_id);
        if (row_id === undefined || row_id === null) {
            console.log("addd to bom");
            var new_row = "<tr id='" + ID + "'><td style='width: 10px'></td><td><input type='text' name='itemCodeBOM[]' value='" + ID + "' readonly></td><td><input type='text' name='itemQtyBOM[]' value='1' readonly></td></tr>";
            $("#fullfilled_items tbody").append(new_row);
        } else {
            console.log("already exists");
            var oldQty = $("#fullfilled_items").find('#' + ID).find("td:eq(2)").find('input').val();

            var newQty = parseInt(oldQty) + 1;
            //  alert(ID + " " +oldQty);

            $("#fullfilled_items").find('#' + ID).find("td:eq(2)").find('input').val(newQty);

        }

    } else return;

 
});
    
</script>