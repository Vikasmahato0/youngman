
<?php include ("includes/header.php");?>
<?php
    $type = $_GET['type'];
    $warehouse = $_GET['warehouse'];


    $sql = '';

    if($type === 'dispatch')     $sql = "SELECT  table_challan.challan_id, table_challan.job_order, table_quotation.delivery_date 
                                        FROM table_challan,table_quotation 
                                        WHERE table_challan.job_order=table_quotation.job_order 
                                        AND table_challan.recieving ='0' AND  table_challan.pickup_loc_id = ? AND table_challan.type = 1";
    else if($type === 'pickup')  $sql = "SELECT  table_challan.challan_id, table_challan.job_order, table_quotation.delivery_date 
                                        FROM table_challan,table_quotation 
                                        WHERE table_challan.job_order=table_quotation.job_order 
                                        AND table_challan.recieving ='0' AND  table_challan.delivery_loc_id = ? AND (table_challan.type = 2 OR table_challan.type = 3)";


    if($qr  = $mysqli->prepare($sql )){
        $qr->bind_param('s', $warehouse);
         $qr->execute();
         $qr->store_result();
         $qr->bind_result($challan_id, $job_order, $delivery_date);  
        
    }else echo $mysqli->error;
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $warehouse." ".$type; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Planning</a></li>
        <li><a href="#">Stats</a></li>
        <li class="active">Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Pending</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            
          </div>
        </div>
        <div class="box-body">
        <table id="orders" class="table table-bordered table-hover">
                <thead>
                <tr>
                
                  <th>Challan Id</th>
                  <th>Job Order</th>
                  <th>Delivery Date</th>
                </tr>
                </thead>
                <tbody>
                  <?php while($qr->fetch()) {?>
                 
                  <tr>
                    <td><?php echo $challan_id; ?></td>
                    <td><?php echo $job_order; ?></td>
                    <td><?php echo  date('d-m-Y', strtotime($delivery_date)); ?></td>
                  </tr>
                  <?php } ?>

                </tbody>
        </table>
       
        </div>
        <!-- /.box-body -->
    
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
