<?php
//$job_order = '2017/Jun/ZECO AIRCON LTD. /16/333/171804006/11';

$job_order = $_POST['job_order'];
if($job_order != '')
getMovementAnalysis($job_order);

function getMovementAnalysis($job_order){
    
    include 'secure/db_connect.php';
    require 'Classes/PHPExcel.php';
    $results = array();
    $results = createTable($job_order);
    
  /*  echo "<br><hr><br>";
    echo "<table>";
    foreach ($results as $key => $row)
    {
        echo "<tr>";
    foreach ($row as $key2 => $val) 
    {
        echo "<td>".$results[$key][$key2]."</td>";
    }
       echo "</tr>";
    }
    
    echo "</table>";*/
    
    $objPHPExcel = new PHPExcel();

  $objPHPExcel->getActiveSheet()->fromArray($results);

  // Redirect output to a clientâ€™s web browser (Excel5)
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="test.xls"');
  header('Cache-Control: max-age=0');
  // If you're serving to IE 9, then the following may be needed
  header('Cache-Control: max-age=1');

  // If you're serving to IE over SSL, then the following may be needed
  header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
  header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
  header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
  header ('Pragma: public'); // HTTP/1.0

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output');  
    
}

function createTable($job_order)
{
    
    include 'secure/db_connect.php';
    $table = array();
    
    $table[0][0] = "Item Code";
    $table[1][0] = " ";
    $table[0][1] = "Description";
    $table[1][1] = " ";
    
    $table[1][2] = "Opening Qty";
    
    $open ="SELECT opening_qty_date FROM location_item_relation WHERE location_id=? LIMIT 1";
    
    if($oq  = $mysqli->prepare( $open )){
        $oq->bind_param('s',$job_order);
        $oq->execute();
        $oq->store_result();
        $oq->bind_result($opening_qty_date);   
        $oq->fetch();
        }//else echo $mysqli->error;
    
    $table[0][2] = $opening_qty_date;
    
    
    $q = "SELECT challan_id, timestamp, type FROM table_challan WHERE job_order = ? ORDER BY timestamp";
     if($items  = $mysqli->prepare( $q )){
        $items->bind_param('s',$job_order);
        $items->execute();
        $items->store_result();
        $items->bind_result($challan_id, $timestamp, $type);   
        }//else echo $mysqli->error;
    
    $j=3;
    while($items->fetch()){
        $ch_type = '';
        $type == '1' ? $ch_type="DISPATCH " : $ch_type="PICKUP ";
        $table[0][$j] = $ch_type.$timestamp;
        $table[1][$j] = $challan_id;
        $j++;
    }
    
    $table[0][$j] = "Closing ".date("Y-m-d h:i:s");
    $table[1][$j] = "Closing Stock";
    
    $table[0][$j+1] = "Damaged";
    $table[1][$j+1] = "-";
    $table[0][$j+2] = "Missing";
    $table[1][$j+2] = "-";
    
    //FILLING TABLE
    
    $query = "SELECT location_item_relation.item_id,table_item.name,  location_item_relation.opening_qty  FROM location_item_relation, table_item WHERE location_item_relation.location_id= ? AND location_item_relation.item_id = table_item.item_code";
    if($items  = $mysqli->prepare( $query )){
        $items->bind_param('s',$job_order);
        $items->execute();
        $items->store_result();
        $items->bind_result($item_id, $item_name, $opening_qty);   
        }//else echo $mysqli->error;
    
   $i=3;
    while($items->fetch()){
        $table[$i][0] = $item_id;
        $table[$i][1] = $item_name;
         $table[$i][2] = $opening_qty;
        $i++;   
    }   
    
    for($col = 2 ; $col<$j; $col++)
    {
        $ch = "SELECT item_id, quantity FROM challan_item_relation WHERE challan_id = ?";
      //  echo "SELECT item_id, quantity FROM challan_item_relation WHERE challan_id =".$table[1][$col]."<br>";
     if($it  = $mysqli->prepare( $ch )){
        $it->bind_param('s', $table[1][$col]);
        $it->execute();
        $it->store_result();
        $it->bind_result($item_id, $quantity);   
        }//else echo $mysqli->error;
        
         while($it->fetch()){
             
             for($row = 2; $row < $i; $row ++){
                 
                 if($table[$row][0]==$item_id)
                     $table[$row][$col] = $quantity;
                 
                 else
                     if(!isset($table[$row][$col]) )  $table[$row][$col] =  " ";
             }
             
    }
        
    }
    
    $closing_stock = "SELECT item_id, quantity, damaged, missing FROM location_item_relation WHERE location_id = ?";
    
     if($cs  = $mysqli->prepare( $closing_stock )){
        $cs->bind_param('s',$job_order);
        $cs->execute();
        $cs->store_result();
        $cs->bind_result($item_id, $quantity,  $damaged, $missing );   
        }//else echo $mysqli->error;
    
    while($cs->fetch()){
        
         for($row = 2; $row < $i; $row ++){
                 
                 if($table[$row][0]==$item_id)
                 {
                     $table[$row][$j] = $quantity;
                     $table[$row][$j+1] = $damaged;
                     $table[$row][$j+2] = $missing;
                 }
                     
                 
                 else
                     if(!isset($table[$row][$j]) )  $table[$row][$j] =  " ";
             }
        
    }
    
   
    
    return $table;
}
?>