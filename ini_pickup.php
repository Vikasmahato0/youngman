<?php
 include 'secure/db_connect.php';

$query = "SELECT job_order, pickup_date FROM table_quotation AS q WHERE pickup_date < DATE_ADD(CURDATE(), INTERVAL 3 DAY) AND status = 'order' AND NOT EXISTS (SELECT pickup_loc_id FROM table_challan AS c WHERE q.job_order = c.pickup_loc_id)";

 if($pick  = $mysqli->prepare( $query )){
      //  $pick->bind_param('s',$job_order);
        $pick->execute();
        $pick->store_result();
        $pick->bind_result($job_order, $pickup_date);   
        }else echo $mysqli->error;
?>
<?php include ("includes/header.php");?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pickups Due
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pickups</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
         <div class="row">

  
  <div class="col-sm-4">  <a href="#manual_modal" class="btn btn-danger" data-toggle="modal">Pickup</a></div>
</div> 
        <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Pickups</h3>
        </div>
          
              <div class="modal" id="manual_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">Add Pickup</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="ajax/manual_pickup.php" method="post" enctype="multipart/form-data">
                                       
                                         <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" autocomplete="off" required>

                                        </div>
                                         <div id="suggesstion-box"></div>

                                        <div class="form-group">
                                             <select class="form-control job col-sm-4" name="job_order" id="job_for_cust">        
                                                <option>Select</option>
                                            </select>

                                        </div>
                                           <div class="form-group">
                                            <label>Select Warehouse</label>
                                                
                                            <select class="form-control" name="warehouse">
                                                <option>Select</option>
                                                
                                                <?php 
                                                $warehouse = "SELECT location_id FROM table_location WHERE location_type='warehouse'";
                                                
                                                if($Items  = $mysqli->prepare( $warehouse )){
                                                    $Items->bind_param('s',$challan_id);
                                                    $Items ->execute();
                                                    $Items ->store_result();
                                                    $Items ->bind_result($loc_id);   

                                                    }else echo $mysqli->error;

                                                while( $Items->fetch()){
                                                    
                                                    echo '<option value="'.$loc_id.'">'.$loc_id.'</option>';
                                                }
                                                $Items->close();
                                                ?>
                                                
                                                
                                            </select>
                                        </div>
                                          <div class="form-group">
                                            <label for="del_date">New Pickup Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="pickup_date" class="form-control pull-right" id="pickup_date" required="true" >
                                            </div>
                                        </div>
                                        
                                      
                                     
                                        <input type="submit" name="submit" value="Add Pickup">
                                    </form>
                                </div>



                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
          <div class="modal" id="extend_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">Extend Order</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="ajax/extend_order.php" method="post" enctype="multipart/form-data">
                                       
                                        <div class="form-group">
                                            <label for="job_order">Job Order</label>
                                            <input class="form-control" type="text" id="job_order" name="jobOrder" value="" required="true" readonly="true">

                                        </div>
                                        <div class="form-group">
                                            <label for="pickup_date">New Pickup Date</label>
                                            <input class="form-control" type="text" id="pickup_date" name="pickup_date" value="" required="true" >

                                        </div>
                                        <input type="file" name="fileToUpload" id="fileToUpload" required="true">

                                    
                                        <input type="submit" name="submit" value="Extend">
                                    </form>
                                </div>



                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="pickup_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">Pickup</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="ajax/initiate_pickup.php" method="post" enctype="multipart/form-data">
                                       
                                        <div class="form-group">
                                            <label for="job_order">Job Order</label>
                                            <input class="form-control" type="text" id="job_order" name="jobOrder" value="" required="true" readonly="true">

                                        </div>
                                         <div class="form-group">
                                            <label>Select Warehouse</label>
                                                
                                            <select class="form-control" name="warehouse">
                                                <option>Select</option>
                                                
                                                <?php 
                                                $warehouse = "SELECT location_id FROM table_location WHERE location_type='warehouse'";
                                                
                                                if($Items  = $mysqli->prepare( $warehouse )){
                                                    $Items->bind_param('s',$challan_id);
                                                    $Items ->execute();
                                                    $Items ->store_result();
                                                    $Items ->bind_result($loc_id);   

                                                    }else echo $mysqli->error;

                                                while( $Items->fetch()){
                                                    
                                                    echo '<option value="'.$loc_id.'">'.$loc_id.'</option>';
                                                }
                                                $Items->close();
                                                ?>
                                                
                                                
                                            </select>
                                        </div>
                                   
                                    
                                        <input type="submit" name="submit" value="Pickup">
                                    </form>
                                </div>



                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
         
          <div class="box-body">
              <table id="godown" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Job Order</th>
                  <th>Pickup Date</th>
                    
                 <th>Action</th>
                    <th>Extend</th>
                  
                </tr>
                </thead>
                <tbody>
          <?php while($pick->fetch()){?>
                      <tr>
                <td><?php echo $job_order; ?></td>
                <td><?php echo $pickup_date; ?></td>
                <td> <a href="#pickup_modal" class="btn btn-success" data-toggle="modal" data-job-order="<?php echo $job_order;?>">Pickup</a></td>
                <td>  <a href="#extend_modal" class="btn btn-warning" data-toggle="modal" data-job-order="<?php echo $job_order;?>">Extend</a></td>
                </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                   <th>Job Order</th>
                  <th>Pickup Date</th>
                    
                 <th>Action</th>
                    <th>Extend</th>
                </tr>
                </tfoot>
              </table>
            </div>
      
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
<script>
  
 $(document).ready(function() {
            $("#name").keyup(function() {
                $.ajax({
                    type: "POST",
                    url: "ajax/readCustomerFromCache.php",
                    data: 'keyword=' + $(this).val(),
                    beforeSend: function() {
                        $("#name").css("background", "#FFF url(img/ LoaderIcon.gif) no-repeat 165px");
                        //console.log($(this).val());
                    },
                    success: function(data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#name").css("background", "#FFF");
                        console.log("data");
                    }
                });
            });
        });

        function selectCustomer(id, name) {
            console.log(name);
            console.log(id);
            $("#id").val(id);
            $("#suggesstion-box").hide();
            $("#name").val(name);
          $.ajax({
                    type: "POST",
                    url: "ajax/get_job_for_customer.php",
                    data: {keyword: id,
                            pickup: 'true'},
                    beforeSend: function() {
                       // $("#name").css("background", "#FFF url(img/ LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function(response) {
                     
                     $("#job_for_cust").empty();
             
                    
                   // $("#job_for_cust").append(response);
                     $("#job_for_cust").append(response);
                
                    }
                });
        }


</script>
