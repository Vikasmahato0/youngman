<?php
include("secure/db_connect.php");
include("secure/functions.php");
sec_session_start();

 error_reporting(E_ALL); ini_set('display_errors', 1);
    // Check connection
    if (mysqli_connect_errno())
      {
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
      }
    echo "<br>connected";
    
    mysqli_autocommit($mysqli, false);
    
    echo "<br>autocommit disabled<br>";
$flag = true;

$challan_id = $_POST['challan_id'];

    $stmt = $mysqli->prepare("DELETE FROM invoice_item_feed WHERE challan_id = ?");
    $stmt->bind_param('s', $challan_id);
    if(! $stmt->execute()){ $flag = false;  echo $st->error; }
    $stmt->close();


    $stmt = $mysqli->prepare("SELECT i.item_id, i.job_order, i.quantity, c.pickup_loc_id FROM challan_item_relation AS i, table_challan AS c WHERE c.challan_id = ? AND c.challan_id = i.challan_id");
    $stmt->bind_param('s', $challan_id);
    $stmt ->execute();
    $stmt ->store_result();
    $stmt ->bind_result($item_id, $job_order, $quantity, $warehouse);  
    
    while($stmt->fetch()){
        
        $add_to_warehouse = "INSERT INTO location_item_relation (location_id, item_id, quantity) VALUES  (?, ?, ? ) ON DUPLICATE KEY UPDATE quantity =( quantity +  VALUES ( quantity))";
        
       if( $st = $mysqli->prepare($add_to_warehouse) ){
        $st->bind_param('sss', $warehouse, $item_id, $quantity);
        if(! $st->execute()){ $flag = false;  echo $st->error; }
        $st->close();
       } else {echo $mysqli->error;}
        
        $remove_from_job = "INSERT INTO location_item_relation (location_id, item_id, quantity) VALUES  (?, ?, ? ) ON DUPLICATE KEY UPDATE quantity =( quantity -  VALUES ( quantity ))";
        
       if( $st = $mysqli->prepare($remove_from_job) ) {
        $st->bind_param('sss', $job_order, $item_id, $quantity);
        if(! $st->execute()){ $flag = false;  echo $st->error; }
        $st->close();
    } else {echo $mysqli->error;}
        
    }

$stmt->close();

if($stmt = $mysqli->prepare("DELETE FROM challan_item_relation WHERE challan_id = ?") ){
$stmt->bind_param('s', $challan_id);
if(! $stmt->execute()){ $flag = false;  echo $stmt->error; }
    $stmt->close();
} else  {echo $mysqli->error; $flag=false;}

if($stmt = $mysqli->prepare("DELETE FROM challan_order_item_relation WHERE challan_id = ?") ){
$stmt->bind_param('s', $challan_id);
if(! $stmt->execute()){ $flag = false;  echo $stmt->error; }
    $stmt->close();
} else  {echo $mysqli->error; $flag=false;}


if( $stmt = $mysqli->prepare("DELETE FROM table_challan WHERE challan_id = ?") ) {
$stmt->bind_param('s', $challan_id);
if(! $stmt->execute()){ $flag = false;  echo $stmt->error; }
    $stmt->close();
} else  {echo $mysqli->error; $flag=false;}

    if ($flag) {
        mysqli_commit($mysqli);
        echo "All queries were executed successfully";
     header("Location: dashboard_planning.php");
    } else {
        mysqli_rollback($mysqli);
        echo "All queries were rolled back";
    } 


    ?>
