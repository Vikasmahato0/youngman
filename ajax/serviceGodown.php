<?php
require('../secure/config.php');
include("../secure/db_connect.php");
if(isset($_POST)){
    $job = $_POST['bookId'];
    $warehouse = $_POST['warehouse'];

    $q = "UPDATE table_quotation SET godown = ? WHERE job_order = ? ";

     if ($insert_stmt = $mysqli->prepare($q))
            {
            $insert_stmt->bind_param('ss', $warehouse, $job);
            
            if(!$insert_stmt->execute()) {
                if(DEBUG) echo $insert_stmt->error."<br><hr>";
                }
            }
          else if(DEBUG) echo $mysqli->error();

}

header ('Location: ../dashboard_planning.php');

?>