<?php
require_once("../secure/db_connect.php");

if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'  )  )
{
if(!empty($_POST["keyword"])) {
    $key = $_POST['keyword'];

$key = strtoupper($key);
    $query = "SELECT item_code, name FROM table_item where UPPER(name) LIKE ? AND bundle = 0";
    if($stmt = $mysqli->prepare($query)){
      $stmt->bind_param('s',  $key); 
                   $stmt->execute(); 
                   $stmt->store_result();
                   $stmt->bind_result($item_code, $name);
?>
<ul id="customer_list">
<?php
while($stmt->fetch()) {
?>
<li onClick="selectItem('<?php echo $item_code; ?>');"> <?php echo $name; ?></li>
<?php } ?>
</ul>
<?php } else echo $mysqli->error;

} }?>
