<?php
include("../secure/db_connect.php");

if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'  ) )
{

$item_id = $_POST['item_id'];
$result = "<br>
              <table class='table table-hover'>
                <tbody>
                <tr>
                  <th>Warehouse</th>
                  <th>Quantity</th>
                  <th>Damaged</th>
                </tr>
";


 $query = "SELECT r.location_id, r.quantity, r.damaged FROM location_item_relation AS r, table_location AS l WHERE l.location_type = 'warehouse' AND r.location_id = l.location_id AND r.item_id = ? ";
if($Items  = $mysqli->prepare( $query )){
 $Items->bind_param('s',$item_id);
 $Items ->execute();
 $Items ->store_result();
 $Items ->bind_result($location, $quantity, $damaged );   
}else echo $mysqli->error;
 while( $Items->fetch()){
$result .= "
            <tr>
                <td>".$location."</td>
                <td>".$quantity."</td>
                <td>".$damaged."</td>
            </tr>";
                                                }
 $result .= "</tbody>
                </table>";
echo $result;
} else echo "Not an ajax request";

?>
