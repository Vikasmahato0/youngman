<?php
include("../secure/db_connect.php");
include("../secure/functions.php");
sec_session_start();
error_reporting(E_ALL); ini_set('display_errors', 1);
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'  ) && login_check($mysqli) )
{ 
$loc_id = $_POST['location'];


$result = "
              <table class='table table-hover'>
                <tbody>
                <tr>
                  <th>Item Code</th>
                  <th>Description</th>
                  <th>Quantity</th>
                </tr>           
";


 $query = "SELECT table_billing.item_code, SUM(table_billing.qty), table_item.name FROM table_billing, table_item WHERE table_billing.job_order = ? AND table_billing.item_code = table_item.item_code ";
                                               
                                                if($Items  = $mysqli->prepare( $query )){
                                                    $Items->bind_param('s',$loc_id);
                                                    $Items ->execute();
                                                    $Items ->store_result();
                                                    $Items ->bind_result($item_id, $name, $quantity);   

                                                    }else echo $mysqli->error;

                                                while( $Items->fetch()){
                                                    $result .= "
                                                    <tr>
                  <td>".$item_id."</td>
                  <td>".$name."</td>
                  <td>".$quantity."</td>
                </tr>
                                                    ";
                                                }

 $result .= "</tbody>
                  </table>";
echo $result;
}

?>