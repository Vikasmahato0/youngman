<?php

include ('secure/db_connect.php');
include ('qb_functions.php');
include ('secure/config.php');

$csrf = $_POST['csrf'];
$challan_id = $_POST['challan_id'];
$job_order = $_POST['job_order'];
$challanBOM = $_POST['challanBOM'];
$itemCodeBOM = $_POST['itemCodeBOM'];
$itemQtyBOM = $_POST['itemQtyBOM'];


if($stmt =  $mysqli->prepare("SELECT qb_id, customer_id, s_no FROM table_quotation WHERE job_order = ?")){
    $stmt->bind_param('s', $job_order);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($qb_id, $customer_id, $s_no);
    $stmt->fetch();
}$stmt->close();

if($stmt =  $mysqli->prepare("SELECT category FROM  customer_local WHERE customer_id = ?")){
    $stmt->bind_param('s', $customer_id);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($category);
    $stmt->fetch();
}$stmt->close();


if($category === "Post"){
    if(DEBUG) echo "<br>qb id = $qb_id, customer id = $customer_id, category = $category";
        if(DEBUG) echo "make invoice";
    if ( recieving_invoice($job_order, $customer_id, $qb_id, $itemCodeBOM, $itemQtyBOM, $s_no, $challan_id) ) {
         if( update_billing_table1( $job_order, $itemCodeBOM, $itemQtyBOM, $challan_id) ) if(DEBUG) echo "Billing updated";
         else   if(DEBUG) echo "could not stop billing";
    }
    else {
    //    header("Location:planning_pickup.php?error='Could not make invoice'");
    }
}


function  recieving_invoice($job_order, $customer_id, $qb_id, $itemCodeBOM, $itemQtyBOM, $s_no, $challan_id){
     require 'secure/getTaxId.php';
    if(DEBUG) echo "$job_order, $customer_id, $qb_id, $itemCodeBOM, $itemQtyBOM, $s_no";
    
    //Make bill from last billed date to recieving date
    require dirname(__FILE__) . '/config.php';
    include 'secure/db_connect.php';
    
  if(!DEBUG)  $rental = getItemID('Rental of  Product'); else $rental = 35;    
   if(!DEBUG)  $mobilisation = getItemID('Mobilisation and Demobilisation Costs'); else  $mobilisation = 36;
    if(!DEBUG) $manpower = getItemID('Manpower Costs'); else $manpower = 37;
    
    if(DEBUG) echo "<br>Rental ".$rental."Mobilisation ".$mobilisation." Manpower ".$manpower."<br>";
    
    $g = "SELECT g.godown_id, q.place_of_supply FROM  godowns AS g, table_quotation AS q WHERE q.job_order = ? AND q.godown = g.godown_name";
    if($stmt = $mysqli->prepare( $g )){
        $stmt->bind_param('s',$job_order);
        $stmt ->execute();
        $stmt ->store_result();
        $stmt ->bind_result($godown_id, $place_of_supply);   
        $stmt->fetch();
    }else echo $mysqli->error;
    
    
    $InvoiceService = new QuickBooks_IPP_Service_Invoice();

    $Invoice = new QuickBooks_IPP_Object_Invoice();
    
    for($i=0; $i<count($itemCodeBOM); $i++){
        
        if($stmt = $mysqli->prepare("SELECT b.unit_price, DATEDIFF( b.last_end_date, c.recieving_date) AS DiffDate, t.HSN FROM table_challan AS c, table_billing AS b, table_item AS t WHERE c.challan_id = ?  AND b.job_order = ? AND b.item_code = ? AND t.item_code = b.item_code LIMIT 1")){
            $stmt->bind_param('sss', $challan_id,  $job_order,  $itemCodeBOM[$i]);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($unit_price,  $billing_period, $hsn);
            $stmt->fetch();
            if(DEBUG) echo $stmt->error;
        }else  if(DEBUG) echo $mysqli->error();
        
        //$stmt->close();
    
    if(DEBUG) echo "SELECT b.unit_price, DATEDIFF( b.last_end_date, c.recieving_date) AS DiffDate FROM table_challan AS c, table_billing AS b WHERE c.challan_id = $challan_id  AND b.job_order = '$job_order' AND b.item_code = '$itemCodeBOM[$i]' LIMIT 1";
     
     
        $billing_period = abs($billing_period);


        if(DEBUG) echo "<br>($unit_price / ".date('t')." ) * ($billing_period + 1)";

        $unit_price = ($unit_price / date("t") ) * ($billing_period + 1);
                    
        $unit_price = number_format((float)$unit_price, 2, '.', '');

        if(DEBUG) echo "<br>SUpplied Unit Price: $unit_price";
    
        $description = $itemCodeBOM[$i];
        
        $ref = $rental;
        
         $txID = getTaxId($place_of_supply, $hsn);
        
        $Line = new QuickBooks_IPP_Object_Line();
                    $Line->setDetailType('SalesItemLineDetail');
                    $Line->setAmount($itemQtyBOM[$i]*$unit_price);
                    if(DEBUG) echo "<br>$itemQtyBOM[$i]*$unit_price<br>";
                    $Line->setDescription($description);
                    
        $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
                    $SalesItemLineDetail->setItemRef($ref);
                    $SalesItemLineDetail->setUnitPrice($unit_price);
                    $SalesItemLineDetail->setQty($itemQtyBOM[$i]);
                    $SalesItemLineDetail->setTaxCodeRef($txID);
                    $Line->addSalesItemLineDetail($SalesItemLineDetail);
                    $Invoice->addLine($Line);    
                }
    
    $Invoice->setCustomerRef( $qb_id );
    $Invoice->setCurrencyRef( 'INR' );
       $Invoice->setDepartmentRef($godown_id);
    $Invoice->setTransactionLocationType($place_of_supply);
    
    
        if ($resp = $InvoiceService->add($Context, $realm, $Invoice))
                        {
                            print('Our new Invoice ID is: [' . $resp . ']');
                            return true;

                        }
                        else
                        {
                            print($InvoiceService->lastError());
                            return false;
                        }  
    
}


function update_billing_table1( $job_order, $itemCodeBOM, $itemQtyBOM, $table_challan_challan_id){
     include 'secure/db_connect.php';
  mysqli_autocommit($mysqli, false);;
  $flag = true;
//ini_set('max_execution_time', 300); 
  for($i=0; $i<count($itemCodeBOM); $i++){
      $reducing_qty = $itemQtyBOM[$i];
      while($reducing_qty > 0){
            $q= "SELECT qty, challan_no FROM table_billing WHERE item_code = ? AND job_order = ? LIMIT 1";
            if(DEBUG) echo "<br>SELECT qty, challan_no FROM table_billing WHERE item_code = '$itemCodeBOM[$i]' AND job_order = '$job_order' LIMIT 1";
            if($stmt = $mysqli->prepare($q)){
            $stmt->bind_param('ss', $itemCodeBOM[$i], $job_order);
             $stmt->execute();
             $stmt->store_result();
             $stmt->bind_result($qty_in_row, $challan_no);
             $stmt->fetch();
             } else $flag = false;

                echo "<br>qty in row: ". $qty_in_row." Challan No: ".$challan_no." Reduciing qty: $reducing_qty Parent ch id: $table_challan_challan_id";   

             if($qty_in_row >= $reducing_qty) {
                 if($s = $mysqli->prepare("UPDATE table_billing SET qty = qty - ? WHERE challan_no = ? AND item_code = ? AND job_order = ?")){
                    $s->bind_param('ssss', $reducing_qty, $challan_no, $itemCodeBOM[$i], $job_order);
                    if( $s->execute() ) $reducing_qty = 0;  else { $flag = false; if(DEBUG) echo $s->error; break;}
                    } else $flag = false;
             }
             else {
                  if($s = $mysqli->prepare("UPDATE table_billing SET qty = 0 WHERE challan_no = ? AND item_code = ? AND job_order = ?")){
                    $s->bind_param('sss', $challan_no, $itemCodeBOM[$i], $job_order);
                    if( $s->execute() ) $reducing_qty = $reducing_qty - $qty_in_row;  else { $flag = false; if(DEBUG) echo $s->error; break;}
                    } else $flag = false;
             }

              if($s = $mysqli->prepare("DELETE FROM table_billing WHERE qty = 0 AND challan_no = ? AND item_code = ? AND job_order = ? ")){
                    $s->bind_param('sss', $challan_no, $itemCodeBOM[$i], $job_order);
                    if( ! $s->execute() ) { $flag = false; if(DEBUG) echo $s->error;}
                    } else $flag = false;
      }
  }

   if($s = $mysqli->prepare("UPDATE table_challan SET status = 2 WHERE challan_id = ? ")){
                    $s->bind_param('s', $table_challan_challan_id);
                    if( ! $s->execute() )  { $flag = false; if(DEBUG) echo $s->error; }
                    } else $flag = false;



    if ($flag) {
        
            mysqli_commit($mysqli);
            if(DEBUG) echo "All queries were executed successfully";
            else  header("Location: dashboard_planning.php?success=1");
        // return true;
        } else {
            mysqli_rollback($mysqli);
            if(DEBUG) echo "All queries were rolled back";
            else header("Location: dashboard_planning.php?error=1");
        //  return false;
        } 

}
?>