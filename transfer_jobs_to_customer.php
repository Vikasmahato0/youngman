<?php

require_once dirname(__FILE__) . '/config.php';
include 'secure/db_connect.php';

/*
* Assuming paren customer_id, job_order are present in table_quotation
*/

 if($Jobs  = $mysqli->prepare("SELECT customer_id, job_order, customer_name FROM transfer_jobs WHERE qb_id = 0")){
    $Jobs ->execute();
    $Jobs ->store_result();
    $Jobs ->bind_result($parentRef, $job_order, $parentName);   
    }else echo $mysqli->error;

while( $Jobs ->fetch()) {
    
    $CustomerService = new QuickBooks_IPP_Service_Customer();

    $JOB = new QuickBooks_IPP_Object_Customer();
    $JOB->setDisplayName($job_order);
    $JOB->setJob(true);
    $JOB->setParentRef($parentRef);
    $JOB->setParentFullName($parentName);

    if ($resp = $CustomerService->add($Context, $realm, $JOB)) {

          $qb_id = QuickBooks_IPP_IDS::usableIDType($resp);
            print('Our new Job ID is: [' . $resp . '] Parent = '.$JOB->getParentFullName().' (name "' . $JOB->getDisplayName() . '")');

            $sql = "INSERT INTO table_quotation (status, customer_id, job_order, qb_id) VALUES ('movement', ?, ?, ?)";

            if($stmt  = $mysqli->prepare($sql)){
                $stmt->bind_param('sss', $parentRef, $job_order, $qb_id );


                if($stmt ->execute()) {
                        $s = $mysqli->prepare("DELETE FROM transfer_jobs WHERE job_order = ?");
                        $s->bind_param('s', $job_order);
                        $s->execute();
                }
               
                }else echo $mysqli->error;


    }else {
        print($job_order." ".$CustomerService->lastError($Context));
        
    }

}



?>