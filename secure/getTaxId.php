<?php


function getTaxId($to_loc, $hsn){
    require('db_connect.php');
    require('config.php');
    if($to_loc === '07')
            $q = "SELECT taxid FROM hsn_tax WHERE place= '07' AND hsn = ? ";
    else
            $q = "SELECT taxid FROM hsn_tax WHERE place= '00' AND hsn = ? ";
    
    if($stmt = $mysqli->prepare( $q )){
        $stmt->bind_param('s',$hsn );
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($taxid);  
         if( ! $stmt->fetch()) if(DEBUG) echo "cannot select ".$stmt->error;
        
        return $taxid;
        
        }else if(DEBUG) echo $mysqli->error;
        
        
}

?>