<?php


include 'db_connect.php';
include 'functions.php';
sec_session_start(); // Our custom secure way of starting a php session. 
 
if(isset($_POST['email'], $_POST['p'])) { 
   $email = $_POST['email'];
   $password = $_POST['p']; // The hashed password.
   if(login($email, $password, $mysqli) == true) {
      // Login success
      
    switch ($_SESSION['role']) {
        case 'sales':
            header("Location: '..\..\..\dashboard_sales.php");
            break;
        case 'sales_coor':
            header("Location: '..\..\..\create_customer.php");
            break;
        case 'sales_head':
            header("Location: '..\..\..\dashboard_sales.php");
            break; 
        case 'planning':
            header("Location: '..\..\..\dashboard_planning.php");
            break;
        case 'movement':
            header("Location: '..\..\..\dashboard_movement.php");
            break;
        case 'finance':
            header("Location: '..\..\..\dashboard_finance.php");
            break;
        case 'ops_head':
             header("Location: '..\..\..\dashboard_ops_head.php");
            break;
        default:
           header("Location: '..\..\..\secure_login.php?error=1");
}
    
   } else {
      // Login failed
       
     header("Location: '..\..\..\secure_login.php?error=1");

   }
} else { 
   // The correct POST variables were not sent to this page.
   echo 'Invalid Request';
}




;?>