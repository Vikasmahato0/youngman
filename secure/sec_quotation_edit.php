<?php

include('functions.php');
include("db_connect.php");
include("../includes/dbcon.php");
include("config.php");
sec_session_start();
//user details
$customer_id        = $_POST['id'];
$customer_name      = $_POST['name'];
$delivery_address   = $_POST['delivery_address'];       if($delivery_address == '') $delivery_address = null;
$delivery_date      = $_POST['del_date'];
$pickup_date        = $_POST['pic_date'];
$user               = $_SESSION['user_email'];

//item details
$item_type          = $_POST['item_type'];
$item_no            = $_POST['itemNo'];
$item_description   = $_POST['itemName'];
$unit_price         = $_POST['price'];
$unit_days          = $_POST['unit_dur'];
$item_qty           = $_POST['quantity'];
$line_price         = $_POST['total'];

//taxes and prices
$row_total          = $_POST['subTotal'];
$freight            = $_POST['freight'];
$sub_total          = $_POST['sub_total_freight'];
$tax                = $_POST['tax'];
$grand_total        = $_POST['totalAftertax'];
$quot_type          = $_POST['quot_type'];
$delivery_pin       = $_POST['deliveryAddPin'];         if($delivery_pin == '') $delivery_pin= null;
$site_name          = $_POST['site_name'];              if($site_name == '' ) $site_name = null;

if ( $delivery_date == '' ) { $delivery_date= null; }
else {

$delivery_date = strtotime($delivery_date);

$delivery_date = date("Y-m-d", $delivery_date);
 if(DEBUG)   echo $delivery_date."<br>";
}

if ($pickup_date == '') {$pickup_date = null;}
else{
    $pickup_date = strtotime($pickup_date);
    $pickup_date = date("Y-m-d", $pickup_date);
    if(DEBUG)  echo $pickup_date."<br>";
}


$contact_name = $_POST['contact_name'];
$security_amt = $_POST['security_amt'];

$billing_address_line_1 = $_POST['billing_address_line_1']; if($billing_address_line_1 == '') $billing_address_line_1 = null;
$billing_address_line_2 = $_POST['billing_address_line_2']; if($billing_address_line_2 == '') $billing_address_line_2 = null;
$billing_city = $_POST['billing_city'];                     if( $billing_city == '') $billing_city = null;
$billing_pincode = $_POST['billing_pincode'];               if( $billing_pincode == '' ) $billing_pincode = null;

if(checkCsrf($_POST['csrf'])){

  mysqli_autocommit($mysqli, false);

    $flag = true;
    

    $s_no = $_POST['s_no'];
    
   if(DEBUG)   echo "Deleting rows";
    $delete1 = " DELETE FROM table_quotation_item WHERE s_no = ? ";
  
    
    if($st = $mysqli->prepare($delete1)){
        $st->bind_param('s', $s_no);
        if ( ! $st ->execute()) $flag = false; else  if(DEBUG) echo $st->error;
        }else { if(DEBUG)  echo $mysqli->error; $flag = false; }
    $st->close;
    

    


  $sql1 = "UPDATE table_quotation SET row_total = ?, sub_total = ?, freight = ?, tax = ?, total = ?, createdby = ?, customer_id = ?, customer_name = ?, delivery_address = ?, delivery_pincode = ?, delivery_date = ?, pickup_date = ?, type = ?, duration_billing = ?, site_name = ?, contact_name = ?, security_amt = ?, billing_address_line_1 = ?, billing_address_line_2 = ?, billing_pincode = ?, billing_city = ? WHERE s_no = ?";
    
    $last_id = $s_no;
    
    if($st = $mysqli->prepare($sql1)){
        $st->bind_param('ssssssssssssssssssssss', $row_total, $sub_total, $freight, $tax, $grand_total, $user,$customer_id, $customer_name, $delivery_address, $delivery_pin, $delivery_date, $pickup_date, $quot_type, $unit_days , $site_name, $contact_name, $security_amt, $billing_address_line_1, $billing_address_line_2, $billing_pincode, $billing_city, $s_no);
        if ( ! $st ->execute()) { $flag = false; if(DEBUG) echo $st->error;} else  {  if(DEBUG)  echo "Record updated successfully."; }
        }else {  if(DEBUG)  echo $mysqli->error; $flag = false; }
    $st->close();

    
 for($i=0;$i<count($item_description);$i++)
 {
  if($item_type[$i]!="" && $item_description[$i]!="" && $item_qty[$i]!="")
  { 
      
       $sql_item = "INSERT INTO table_quotation_item (s_no, type, item_code, `desc` , unit_price, qty, tot) VALUES (?, ?, ?, ?, ?, ?, ?)";
     // echo $sql1;
      if($st = $mysqli->prepare($sql_item)){
        $st->bind_param('sssssss', $last_id, $item_type[$i], $item_no[$i], $item_description[$i], $unit_price[$i], $item_qty[$i], $line_price[$i]);
        if ( ! $st ->execute()) { $flag = false;  if(DEBUG)  echo $st->error."<br>".$sql1; } else  {  if(DEBUG)   echo "items inserterd"; }
        }else { if(DEBUG)  echo $mysqli->error."<br>".$sql1; $flag = false; }
    $st->close();
      
     if(DEBUG)  if(DEBUG)   echo "items inserterd";
  }
 }


if($flag == true) {
     mysqli_commit($mysqli);
    echo 'viewquotation.php?id='.$last_id;
    // header('location: ../viewquotation.php?id='.$last_id);
}else {
    mysqli_rollback($mysqli);
      echo "Error";
}
    
    
 // 

}
else {

  echo "CRSF check failed!!";
}

?>