<?php include ("includes/header.php");?>
<?php 
error_reporting(E_ALL); ini_set('display_errors', 1);

if( checkCsrf($_POST['csrf']) && $_SESSION['role']==='planning' ){

$location = $_POST['warehouse'];
$job_order = $_POST['bookId'];
$challanType = $_POST['challanType'];

if($challanType=='Sales'){
    $challan_type='0';
}elseif($challanType=='Rental'){
     $challan_type='1';
}elseif($challanType=='Pickup'){
     $challan_type='2';
}elseif($challanType=='Return'){
     $challan_type='3';
}

$location_from = '';
$location_to  = '';

if($challan_type == '2' || $challan_type == '3'){
   $location_from = $job_order;
    $location_to  = $location; 
}else{
     $location_from = $location;
    $location_to  = $job_order; 
}

$select_feed = "SELECT order_item_feed.item_code, table_item.bundle ,order_item_feed.qty, table_item.name, table_item.value FROM table_item ,order_item_feed WHERE table_item.item_code=order_item_feed.item_code AND order_item_feed.job_order = ?";
    if($stmt = $mysqli->prepare($select_feed)){
        $stmt->bind_param('s', $job_order);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($item_code, $bundle,  $qty, $name, $value );
        }else echo $mysqli->error;
       
    $sel =  "SELECT order_item_feed.item_code, table_item.bundle ,order_item_feed.qty, table_item.name, table_item.value FROM table_item ,order_item_feed WHERE table_item.item_code=order_item_feed.item_code AND order_item_feed.job_order = '$job_order'";

?>
<div class="modal modal-danger" id="verify" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>
                <input type="text" name="password" id="password">
                <button type="submit" name="verify_pass" class="btn btn-primary" data-dismiss="modal" onclick="verifyUser(this.form.password);">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </form>
        </div>
    </div>
</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
        <br>
          <strong>From Location:</strong><p id="from_location"><?php echo $location_from;?></p><br>
	          <strong>To Location:</strong> <?php echo $location_to;?><br>
	          <strong>Job Order:</strong> <?php echo $job_order;?><br>
        
        
    </section>

    <!-- Main content -->
    <section class="content">
        <form role="form" method="post" action="add_gui_challan.php" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo $_SESSION['login_string']; ?>" name ="csrf" > 
            <!-- Left col -->
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Challan BOM</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class='row'>

                                <table class="table table-striped table-hover" id="table_challan_rental">
                                    <thead>
                                        <tr>
                                            <th width="2%">
                                                <input id="check_all" class="formcontrol" type="checkbox" />
                                            </th>
                                            <th width="10%">Item No</th>
                                            <th width="40%">Description</th>
                                            <th width="10%">Unit Price</th>
                                            <th width="8%">Quantity</th>
                                            <th width="8%">Available Qty</th>
                                            <th width="10%">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>

                            <div class='row'>
                                <div class='col-xs-12 col-sm-3 col-md-3 col-lg-3'>
                                    <button class="btn btn-danger delete" type="button">- Delete</button>
                                    <button class="btn btn-success addmore" type="button">+ Add More</button>
                                </div>

                                <div class="col-md-6" style="float:right;">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"> Total: ₹</div>
                                            <input type="number" step="any" class="form-control" name="subTotal" id="subTotal" placeholder="Subtotal" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" readonly>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- ./col -->
                <div class="col-md-4">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Material to be sent</h3>
                            
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-striped required_items" id="required_items">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Item ID</th>
                                        <th>Type</th>
                                        <th>Qty</th>
                                        <th>Add</th>
                                    </tr>
                                    <?php $s_no = 0; while($stmt->fetch())  {
                        $s_no++;
                            $bundle ? $bundle="Bundle" : $bundle = "Item"; 
                            echo ' <tr id="'.$item_code.'">
                              <td>'.$s_no.'</td>
                              <td>'.$item_code.'</td>
                              <td>'.$bundle.'</td>
                              <td> '.$qty.'</td>
                              <td style="display:none;">'.$name.'</td>
                              <td style="display:none;">'.$value.'</td>
                              <td><button type="button" class="btnSelect btn-primary">Add</button></td>
                            </tr>';
                
                }?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Material fullfilled by present BOM</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-striped" id="fullfilled_items">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Item ID</th>
                                        <th>Quantity</th>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- ./col -->
            </div>
            <!-- /.col -->
            <input type="hidden" name="from" value="<?php echo $location_from; ?>" />
            <input type="hidden" name="job_order" value="<?php echo $job_order; ?>" />
            <input type="hidden" name="to" value="<?php echo $location_to; ?>" />
            <input type="hidden" name="typeofchallan" value="<?php echo $challan_type; ?>" />
            <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
        </form>

        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php 

}
include ("includes/footer.php"); ?>
<!--<script src="dist/js/gui_planning.js"></script>-->
<script>
$(".delete").on('click', function() {
	$('.case:checkbox:checked').parents("tr").remove();
	$('#check_all').prop("checked", false); 
	
});
    
    var from_location = $("#from_location").text();

function verifyUser(password) {
    if (password.value == "abcd") addEmptyRow();
    else alert("Wrong password");
}


$(".addmore").on('click', function () {
    $('#verify').modal('show');
});

var length = $('#table_challan_rental').length;

/*$(document).on('change keyup blur', '.search', function () {
    var keyword = $(this).val();
    var type = '';
    id_arr = $(this).attr('id');
    id = id_arr.split("_");
    if(id[0]=="itemNo") { type = "item_no";}
    if(id[0]=="itemName") { type = "item_name";}
    
    //$('#desc_'+id[1]).val(desc[itemCode]);  
   
      $.ajax({
            type: "POST",
            url: "ajax/getItemCustomPlanning.php",
            data: {
                keyword: keyword,
                type: type
            },

            success: function (data) {
                console.log(data);
              //  avail_qty = (+data); //because typeof (+data) is int
            //    addRow(ID, desc, price, 1, avail_qty);
                calculateTotal()
            }
        });
});*/
    
    $(document).on('focus','.autocomplete_txt',function(){
	type = $(this).data('type');
	
	if(type =='productCode' )autoTypeNo=0;
	if(type =='productName' )autoTypeNo=1; 	
	
	$(this).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url : 'ajax/getItemCustomPlanning.php',
				dataType: "json",
				method: 'post',
				data: {
				   keyword: request.term,
				   type: type
				},
				 success: function( data ) {
					 response( $.map( data, function( item ) {
					 	var code = item.split("|");
						return {
							label: code[autoTypeNo],
							value: code[autoTypeNo],
							data : item
						}
					}));
				}
			});
		},
		autoFocus: true,	      	
		minLength: 0,
		appendTo: "#modal-fullscreen",
		select: function( event, ui ) {
			var names = ui.item.data.split("|");
			id_arr = $(this).attr('id');
	  		id = id_arr.split("_");
	  		console.log(names, id);
	  		
			$('#itemNo_'+id[1]).val(names[0]);
			$('#itemName_'+id[1]).val(names[1]);
			//$('#quantity_'+id[1]).val(1);
			//$('#price_'+id[1]).val(names[2]);
			//$('#total_'+id[1]).val( 1*names[2] );
			calculateTotal();
		}		      	
	});
});
    
    var specialKeys = new Array();
specialKeys.push(8,46); //Backspace
function IsNumeric(e) {
    var keyCode = e.which ? e.which : e.keyCode;
    console.log( keyCode );
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    return ret;
}
    

function addEmptyRow() {

    //   length = length + 1;
    //  alert(length);
    var html = "<tr id='' class = 'item'> <td><input class='case' type='checkbox'/></td>" +
        "  <td> <input type='text' data-type='productCode' name='itemNo[]' id='itemNo_" + length + "' class='form-control autocomplete_txt search' autocomplete='off' value='' readonly></td>" +
        "<td> <input type='text' data-type='productName' name='itemName[]' id='itemName_" + length + "' class='form-control autocomplete_txt search' autocomplete='off' value=''></td>" +
        "  <td><input type='number' name='price[]' id='price_" + length + "' class='form-control changesNo m_price' autocomplete='off' value=''></td>" +
        "<td><input type='number' name='quantity[]' id='quantity_" + length + "' class='form-control changesNo m_quantity' autocomplete='off' onkeypress='return IsNumeric(event); ondrop='return false;' onpaste='return false;' value='1'></td>" +
        "<td><input type='number' name='quantity_aval[]' id='quantity_aval_" + length + "' class='form-control changesNo quantity' autocomplete='off' onkeypress='return IsNumeric(event); ondrop='return false;' onpaste='return false;' value='' readonly></td>" +
        " <td><input type='number' name='total[]' id='total_" + length + "' class='form-control totalLinePrice' autocomplete='off' onkeypress='return IsNumeric(event);'ondrop='return false;' onpaste='return false;' value='' readonly></td>" + "</tr>";

    $("#table_challan_rental tbody").append(html);

    length++;
}
    
    $(document).on('change keyup blur','.m_price',function(){
	id_arr = $(this).attr('id');
	id = id_arr.split("_");
	quantity = $('#quantity_'+id[1]).val();
	price = $('#price_'+id[1]).val();
	if( quantity!='' && price !='') $('#total_'+id[1]).val( (parseFloat(price)*parseFloat(quantity)).toFixed(2) );	
	calculateTotal();
});
    $(document).on('change keyup blur','.m_quantity',function(){
	id_arr = $(this).attr('id');
	id = id_arr.split("_");
	quantity = $('#quantity_'+id[1]).val();
	price = $('#price_'+id[1]).val();
	if( quantity!='' && price !='') $('#total_'+id[1]).val( (parseFloat(price)*parseFloat(quantity)).toFixed(2) );	
	calculateTotal();
});
    

$("#required_items").on('click', '.btnSelect', function () {
    // alert("clicked");

    var currentRow = $(this).closest("tr");
    var ID = currentRow.find("td:eq(1)").text();
    var bundle = currentRow.find("td:eq(2)").text();
    var qty = currentRow.find("td:eq(3)").text();
    var desc = currentRow.find("td:eq(4)").text();
    var price = currentRow.find("td:eq(5)").text();

    // alert(desc);
    qty = parseInt(qty, 10);
    if (qty > 0) {
        currentRow.find("td:eq(3)").text(qty - 1);

        var row_id = $("#fullfilled_items").find('#' + ID).html();
        //   alert(row_id);
        if (row_id === undefined || row_id === null) {
            console.log("addd to bom");
            var new_row = "<tr id='" + ID + "'><td></td><td><input type='text' name='itemCodeBOM[]' value='" + ID + "' readonly></td><td><input type='text' name='itemQtyBOM[]' value='1' readonly></td></tr>";
            $("#fullfilled_items tbody").append(new_row);
        } else {
            console.log("already exists");
            var oldQty = $("#fullfilled_items").find('#' + ID).find("td:eq(2)").find('input').val();

            var newQty = parseInt(oldQty) + 1;
            //  alert(ID + " " +oldQty);

            $("#fullfilled_items").find('#' + ID).find("td:eq(2)").find('input').val(newQty);

        }

    } else return;

    if (bundle == "Bundle") {
        $.ajax({
            type: "POST",
            url: "expandBundle.php",
            data: {
                keyword: ID,
                location_id: from_location
            },

            success: function (data) {

                console.log(data);

                var obj = JSON.parse(data);
                console.log(obj);

                for (var prop in obj) {
                    addRow(obj[prop].item_code, obj[prop].description, obj[prop].value, obj[prop].quantity, obj[prop].avail_qty);

                }
                calculateTotal()
            }
        });



    } else {
        var avail_qty = '';
        $.ajax({
            type: "POST",
            url: "getAvailItemQty.php",
            data: {
                keyword: ID,
                location_id: from_location
            },

            success: function (data) {
                console.log(data);
                avail_qty = (+data); //because typeof (+data) is int
                addRow(ID, desc, price, 1, avail_qty);
                calculateTotal()
            }
        });
    }
});

$(document).on('change keyup blur', '.changesNo', function () {
    var parent_id = $(this).closest('tr').attr('id');
    var qty = $("#table_challan_rental").find('#' + parent_id).find("td:eq(4)").find('input').val();

    var price = $("#table_challan_rental").find('#' + parent_id).find("td:eq(3)").find('input').val();
    qty = parseInt(qty);
    price = parseFloat(price).toFixed(2);
    var new_price = qty * price;
    $("#table_challan_rental").find('#' + parent_id).find("td:eq(6)").find('input').val(new_price);
    calculateTotal();
});

function calculateTotal() {
    subTotal = 0;
    $('.totalLinePrice').each(function () {
        if ($(this).val() != '') subTotal += parseFloat($(this).val());
    });
    $('#subTotal').val(subTotal.toFixed(2));
}

function addRow(ID, desc, price, quantity, avail_qty) {
    console.log(ID + desc + price + quantity);
    var ids = $("#table_challan_rental").find('#' + ID).html();
    var totLinePrice = parseFloat(price * quantity).toFixed(2);
    if (ids === undefined || ids === null) {



        var html = "<tr id='" + ID + "' class = 'item'> <td><input class='case' type='checkbox'/></td>" +
            "  <td> <input type='text' data-type='productCode' name='itemNo[]' id='itemNo_0' class='form-control autocomplete_txt' autocomplete='off' value='" + ID + "' readonly></td>" +
            "<td> <input type='text' data-type='productName' name='itemName[]' id='itemName_0' class='form-control autocomplete_txt' autocomplete='off' value='" + desc + "'></td>" +
            "  <td><input type='number' name='price[]' id='price_0' class='form-control changesNo' autocomplete='off' onkeypress='return IsNumeric(event);' ondrop='return false;' onpaste='return false;' value='" + price + "'></td>" +
            "<td><input type='number' name='quantity[]' id='quantity_0' class='form-control changesNo' autocomplete='off' onkeypress='return IsNumeric(event); ondrop='return false;' onpaste='return false;' value='" + quantity + "'></td>" +
            "<td><input type='number' class='form-control quantity_aval' autocomplete='off' onkeypress='return IsNumeric(event); ondrop='return false;' onpaste='return false;' value='" + avail_qty + "' readonly></td>" +
            " <td><input type='number' name='total[]' id='total_0' class='form-control totalLinePrice' autocomplete='off' onkeypress='return IsNumeric(event);'ondrop='return false;' onpaste='return false;' value='" + totLinePrice + "' readonly></td>" + "</tr>";

        $("#table_challan_rental tbody").append(html);
    } else {
        var oldQty = $("#table_challan_rental").find('#' + ID).find("td:eq(4)").find('input').val();
        var newQty = parseInt(oldQty) + parseInt(quantity);
        $("#table_challan_rental").find('#' + ID).find("td:eq(4)").find('input').val(newQty);
        var total = newQty * parseFloat(price).toFixed(2);
        $("#table_challan_rental").find('#' + ID).find("td:eq(6)").find('input').val(total);

    }
}
    
</script>
