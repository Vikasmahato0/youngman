<?php include ("includes/header.php");?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content" id="content">
     
         <div id="loader" style="display:none;"></div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Challans </h3>
                    </div>
                    <!-- /.box-header -->



                    <div class="modal" id="my_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">Add Recieving</h4>
                                </div>
                                <div class="modal-body">
                                    <form method="post" id="myForm" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="challan">Challan ID</label>
                                            <input class="form-control" type="text" id="challan" name="bookId" value="" required="true" readonly>

                                        </div>
                                        <div class="form-group">
                                            <label for="job_order">Job Order</label>
                                            <input class="form-control" type="text" id="job_order" name="jobOrder" value="" required="true" readonly>

                                        </div>
                                        <div class="form-group">
                                            <label for="fileToUpload">Recieving</label>
                                        <input type="file" name="fileToUpload" id="fileToUpload" accept=".pdf,.jpg, .jpeg, .png, .gif" required="true">
                                        </div>
                                        
                              <!--          <div class="form-group">
                                            <label for="transporter">Transporter</label>
                                            <input class="form-control" type="text" name="transporter" id="transporter" placeholder="Transporter Name" required="true">

                                        </div> -->

                                          <div class="form-group">
                                            <label>Select Transporter</label>
                                            <select class="form-control" name="transporter" id="transporter" required="true">
                                                
                                                <option>Select</option>
                                                
                                                <?php 
                                                $vendors = "SELECT vendor_id, vendor_name FROM table_vendors";
                                                
                                                if($transporters = $mysqli->prepare( $vendors )){
                                                    $transporters ->execute();
                                                    $transporters ->store_result();
                                                    $transporters ->bind_result($vendor_id, $vendor_name);   

                                                    }else echo $mysqli->error;

                                                while( $transporters->fetch()){
                                                    
                                                    echo '<option value="'.$vendor_id.'">'.$vendor_name.'</option>';
                                                }
                                                
                                                ?>
                                                
                                                
                                            </select>
                                        </div>


                                        <div class="form-group">
                                            <label for="name">Recieving Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>

                                                <input class="form-control" name="recieving_date" data-inputmask="'alias': 'dd/mm/yyyy'" id="datemask" data-mask="" type="text" class="form-control pull-right">
                                            </div>
                                        </div>                                         

                                        <div class="form-group">
                                            <label for="gr_no">LR No</label>
                                            <input class="form-control" type="text" name="gr_no" id="gr_no" placeholder="LR Number" required="true">

                                        </div>
                                        <div class="form-group">
                                            <label for="amt">Transport Amount</label>
                                            <input class="form-control" type="text" name="amt" id="amt" placeholder="Transport Amount" required="true">

                                        </div>
                                        <input type="submit" name="submit" id="addRecieving" value="Add Recieving">
                                    </form>
                                </div>



                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="box-body">
                        <table id="godown" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Type of challan</th>
                                    <th>Challan No</th>
                                    <th>Delivery/Pickup Date</th>
                                    <th>Job Order</th>
                                    <th>View</th>
                                    <th>Upload Recieving</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                              //  echo $_SESSION['username'];
             
                      $sql = "SELECT table_challan.type, table_challan.challan_id, table_challan.job_order,table_quotation.delivery_date, table_quotation.pickup_date FROM table_challan,table_quotation WHERE table_challan.job_order=table_quotation.job_order AND table_challan.status=0 AND ( table_challan.pickup_loc_id = ? OR table_challan.delivery_loc_id = ?)";
                        if($stmt = $mysqli->prepare($sql)){
                            $stmt->bind_param('ss',   $_SESSION['username'],  $_SESSION['username']);
                            $stmt->execute();
                            $stmt->store_result();
                            $stmt->bind_result($type, $challan_id, $job_order, $delivery_date, $pickup_date);
                        }else echo $mysqli->error;
                       
		           		while ($stmt->fetch()) {
                ?>
                                    <tr>

                                        <td>
                                            <?php if($type=="1") echo "<h4><span class='label label-success'>Delivery</span></h4>";
                            else if($type=='2') echo "<h4><span class='label label-danger'>Pickup</span></h4>"; 
                                            else if($type=='3')  echo "<h4><span class='label label-warning'>Pickup</span></h4>" ?></td>
                                        <td>
                                            <?php echo $challan_id; ?>
                                        </td>
                                        <td>
                                        
                                             <?php if($type==="1") echo $delivery_date;
                                                else if($type==='2') echo $pickup_date; 
                                                else if($type==='3')  echo $pickup_date; ?>

                                        </td>
                                        <td>
                                            <?php echo $job_order; ?>
                                        </td>
                                        <td>
                                             <?php if($type=="1" || $type=='3') { ?>
                                            <form action="view_challan.php?id=<?php echo $job_order; ?>" method="post">

                                                <input type="hidden" name="challan_id" id="challan_id" value="<?php echo $challan_id; ?>">

                                                <button class="btn btn-info" type="submit" name="formpdf_btn"><i class="fa fa-print"></i>  Challan</button>
                                            

                                                
                                            </form>
                         <?php }else { ?>
                                            <button type="button" class = "items btn btn-info" data-job="<?php echo $job_order; ?>">Items</button>
                                            <?php } ?>

                                        </td>
                                        <td>
                                             <?php if($type=="1") { ?>
                                            <a href="#my_modal" data-toggle="modal" data-book-id="<?php echo $challan_id; ?>" data-job-order="<?php echo $job_order;?>">Add Recieveing</a>
                                            <?php }else { ?>
                                            
                                             <form action="recieving_form.php" method="post">
                                                <input type="hidden" name="job_order" id="job_order" value="<?php echo $job_order; ?>">
                                                <input type="hidden" name="challan_id" id="challan_id" value="<?php echo $challan_id; ?>">

                                                <button class="btn btn-info" type="submit" name="recieve_btn">Complete Pickup </button>
                                               

                                            </form>
            
                                            
                                            <?php } ?>

                                        </td>
                </tr>
                <?php 
                }
                ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Type of challan</th>
                        <th>Challan No</th>
                        <th>Delivery/Pickup Date</th>
                        <th>Job Order</th>
                        <th>View Challan</th>
                        <th>Upload Recieving</th>
                    </tr>
                </tfoot>
                </table>
            </div>


        </div>
        <!-- /.box -->
</div>
<!-- /.col -->

<!-- /.col -->
</div>
<!-- /.row -->
</section>


<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>

<!-- If recieving successfull show everything ok info -->
                      <?php if(isset($_GET['success'])) {?>
                        <script>
                        showAlert("Success", "Recieving Added", "success");
                        </script>
                        <?php }?>

                    <!-- if registration error show this -->
                        <?php if(isset($_GET['error'])) {?>
                        <script>
                        showAlert("Error", "Oops! Some error ocurred.", "error");
                        </script>
                        <?php }?>  
<script>
    
    $('.items').click(function() {

        var job = $(this).data('job');      
	job = encodeURI(job).replace(/&/g, "%26"); 
       console.log(job);   
        $.ajax({
            "type":"POST",
            "url":"ajax/get_material.php",
            data:'location='+job,
            "success":function(data){
                //$("#location_details")
                   // .html(data);
                var win=window.open('about:blank');
        with(win.document)
        {
            open();
            write(data);
            close();
        }
            }
        });
} );


</script>

<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
 $("#datemask").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
 </script>
<script src="plugins/jQuery/jQuery-validate.js"></script>

<script type="text/javascript">
		$.validator.setDefaults( {
			submitHandler: function (form) {
                    var formData = new FormData( form );
 //action="add_recieving.php"
    console.log(formData);
    $.ajax({
       // url: window.location.pathname,
        url: 'add_recieving.php',
        type: 'POST',
        data: formData,
       // async: false,
        beforeSend: function() {
         document.getElementById("loader").style.display = "block";
            $('#my_modal').modal('hide');
        },
        
        success: function (data) {
            console.log(data);
            location.reload(); 
        },
        error: function(data){
            console.log(data);
        },
        cache: false,
        contentType: false,
        processData: false
    }).complete(function() {
             document.getElementById("loader").style.display = "none";
        });

    return false;

     
			}
		} );

		$( document ).ready( function () {
			$( "#myForm" ).validate( {
				rules: {
					bookId: "required",
					jobOrder: "required",
                    transporter: "required",
                    recieving_date: "required",
                    gr_no: {
                      required: true,
                       maxlength: 10
                   },
                   amt: {
                       required: true,
                       maxlength: 10,
                       digits: true
                   }
				},
				messages: {
					transporter: "Please enter Transporter Name",
                    recieving_date: "Please enter Date of Recieving",
                    gr_no: "Please enter GR No",
                    amt: "Please enter Amount"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
			} );

		
		} );
	</script>

    <script>
setTimeout(function(){
   window.location.reload(1);
}, 60000);
</script>
