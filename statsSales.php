
<?php include ("includes/header.php");?>
<?php
  $q  = $mysqli->prepare("SELECT email FROM members WHERE role IN ('sales', 'sales_head', 'sales_coor')" );
  $q->execute();
  $q->store_result();
  $q->bind_result($email);   
      

?>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Stats
        <small>it all starts here</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
       
        <li class="active">Stats</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
           
          </div>
        </div>
        <div class="box-body">
            <table id="orders" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Email</th>
                  <th>Quotations</th>
                  <th>Orders</th>
             
                  <th>Movement</th>
                </tr>
                </thead>
                <tbody>
      <?php   while( $q->fetch()){ 

                   $quotations  = $mysqli->prepare("SELECT COUNT(s_no) FROM table_quotation WHERE createdby = ? AND status = 'quot'" );
                   $quotations->bind_param('s', $email);
                   $quotations->execute();
                   $quotations->store_result();
                   $quotations->bind_result($quot);  
                  $quotations->fetch();

                   $planning  = $mysqli->prepare("SELECT COUNT(s_no) FROM table_quotation WHERE createdby = ? AND status = 'order'" );
                   $planning->bind_param('s', $email);
                   $planning->execute();
                   $planning->store_result();
                   $planning->bind_result($order);  
                   $planning->fetch();

                  $movement  = $mysqli->prepare("SELECT COUNT(DISTINCT(c.job_order)) from table_challan AS c, table_quotation AS q WHERE c.job_order = q.job_order AND c.type=1 AND c.recieving='0' AND q.createdby = ?" );
                   $movement->bind_param('s', $email);
                   $movement->execute();
                   $movement->store_result();
                   $movement->bind_result($movement);  
                   $movement->fetch();
      
      ?>
                <tr>
                 <td><?php echo $email; ?></td>
                 <td><a href = "detailed_sales.php?pending=quot&email=<?php echo $email; ?>" ><?php echo $quot; ?> </a></td>
                 <td><a href = "detailed_sales.php?pending=order&email=<?php echo $email; ?>" ><?php echo $order; ?></a></td>
       
                 <td><a href = "detailed_sales.php?pending=movement&email=<?php echo $email; ?>" ><?php echo $movement; ?></a></td>
                </tr>             
                                                  
      <?php         } ?>
                                                
               
                </tbody>
               
              </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
      
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>