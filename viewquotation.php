<?php include("includes/header.php"); ?>

<?php
$s_no = $_GET['id'];

$sql = "SELECT q.row_total, q.sub_total, q.freight, q.tax, q.total, q.createdby, q.customer_id, q.customer_name, q.delivery_address, q.delivery_date, q.duration_billing, q.billing_address_line_1, q.billing_address_line_2,q.billing_city , q.billing_pincode, l.mailing_address, l.mailing_pincode, q.contact_name, q.security_amt FROM table_quotation AS q, qb_cache_customer AS c, customer_local AS l WHERE q.s_no = ? AND c.customer_id = q.customer_id AND c.customer_id = l.customer_id";
if($info = $mysqli->prepare($sql)){
$info = $mysqli->prepare($sql);
$info->bind_param('s', $s_no);
$info->execute();
$info->store_result();
$info->bind_result( $row_total, $sub_total, $freight, $tax, $total, $created_by, $customer_id, $customer_name, $delivery_address, $delivery_date, $duration_billing, $billing_address_line_1, $billing_address_line_2, $billing_city, $billing_pincode, $mailing_address, $mailing_pincode, $contact_name, $security_amt);
$info->fetch();
}else echo "prepare failed".$mysqli->error;

$string = ' ';

 $billing_address = $billing_address_line_1."<br>".$billing_address_line_2."<br>".$billing_city."<br>".$billing_pincode;

$stmt =  $mysqli->prepare("SELECT `type`, `item_code`, `desc`, `unit_price`, `qty`, `tot`  FROM table_quotation_item WHERE s_no = ?");

if($stmt){
  //  echo "prepared";
      $stmt->bind_param('s',  $s_no); 
    $stmt->execute(); // Execute the prepared query.
    $stmt->store_result();
    $stmt->bind_result($type, $item_code, $desc, $unit_price, $qty, $tot); // get variables from result
     $si = 0;
    while($stmt->fetch()) {
        $si++;
     $string.="<tr>
     <td>$si</td>
     <td>$item_code</td>
     <td>$desc</td>
     <td>$unit_price/Month</td>
     <td>$qty</td>
     <td>$tot</td>
     </tr>";
    }
    
}else echo "prepare failed".$mysqli->error;


?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Quotation
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Quotation</a></li>
                <li class="active">View Quotation</li>
            </ol>
        </section>

        <div class="pad margin no-print">
            <div class="callout callout-info" style="margin-bottom: 0!important;">
                <h4><i class="fa fa-info"></i> Note:</h4>
                This page has been enhanced for printing. Click the print button at the bottom of the quotation to test.
            </div>
        </div>

        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img src="logo.png">
                        <medium class="pull-right">Rental Quotation</medium>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div>
                <strong>Customer ID:</strong>
                <?php echo $customer_id; ?><br>
                <strong>Customer Name:</strong>
                <?php echo $customer_name; ?>

            </div>
            <hr>

            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">

                    <strong> Kind Attention</strong>
                    <address>
             <?php echo $contact_name."<br>".$mailing_address."<br>".$mailing_pincode;?>
          </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <strong>  Delivery Address</strong>
                    <address>
            <?php echo $delivery_address;?>
          </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <strong>  Billing Address</strong>
                    <address>
            <?php echo $billing_address;?>
          </address>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Item Code</th>
                                <th>Description</th>

                                <th>Unit Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $string; ?>
                            <tr>
                                <th>Total</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>₹
                                    <?php echo $row_total;?>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->

                <div class="col-xs-6 text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    <p>
                        <b>Terms and Conditions</b>
                        <ol>
                            <li>Payment: 100% alongwith the work order.</li>
                            <li>Any other govt levy, or local govt entry charge shall be charged extra if any</li>
                            <li>YOUNGMAN STANDARD HIRE TERMS must be signed, acknowledged and sent to us in order to process the material and commence the rental of the aforesaid.</li>
                            <li>Security cheque of amount : INR  <?php echo $security_amt; ?>/- must be provided by you. Kindly note that this cheque can be post dated also. This is only for security purpose and shall not be encashed by YOUNGMAN INDIA PVT. LTD. under normal circumstances.</li>
                            <li>Any damage in the material shall be treated in accordance with the Rental Agreement/YOUNGMAN STD. HIRE TERMS.</li>
                        </ol>
                    </p>
                </div>
                <!-- /.col -->
                <div class="col-xs-6">
                    <!--<p class="lead">Amount Due 2/22/2014</p>-->

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">Freight:</th>
                                <td>₹
                                    <?php echo $freight;?>
                                </td>
                            </tr>
                            <tr>
                                <th>Subtotal:</th>
                                <td>₹
                                    <?php echo $sub_total;?>
                                </td>
                            </tr>
                            <tr>
                                <th>Tax</th>
                                <td>₹
                                    <?php echo $tax;?>
                                </td>
                            </tr>
                           
                            <tr>
                                <th>Total:</th>
                                <td>₹
                                    <?php echo $total;?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    <a href="printquotation.php?id=<?php echo $s_no; ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                </div>
            </div>
        </section>
        <!-- /.content -->
        <div class="clearfix"></div>
    </div>
    <!-- /.content-wrapper -->
    <?php include("includes/footer.php"); ?>