<?php include("includes/header.php"); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <div id="loader" style="display:none;"></div>
    <script src="dist/js/readCustomer.js"></script>

    <section class="content-header">
        <h1>
            New Rental Quotation
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">New Rental Quotation</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">

                        <form role="form" method="post"  id="myForm">

<input type="hidden" name="csrf" value="<?php echo $_SESSION['login_string']; ?>" >
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="id">Customer ID</label>
                                            <input type="text" class="form-control" name="id" id="id" placeholder="Enter ID" autocomplete="off" required readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name (required)" autocomplete="off" required>

                                        </div>
                                        <div id="suggesstion-box"></div>
                                        <div class="form-group">
                                            <label for="del_date">Delivery Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="del_date" class="form-control pull-right" id="del_date">
                                            </div>
                                        </div>
                                          <p class="hidden del_date_buttons">
+                                            <button type="button" onClick="addToPickDate(2)">2 Months</button>
+                                            <button type="button" onClick="addToPickDate(3)">3 Months</button>
+                                            <button type="button" onClick="addToPickDate(4)">4 Months</button>
+                                            <button type="button" onClick="addToPickDate(5)">5 Months</button>
+                                            <button type="button" onClick="addToPickDate(6)">6 Months</button>
+                                        </p>
                                        <div class="form-group">
                                            <label for="site_name">Site Name</label>
                                            <input type="text" class="form-control" name="site_name" id="site_name" placeholder="Site Name">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="contact_name">Contact Name</label>
                                            <input type="text" class="form-control" name="contact_name" id="contact_name" placeholder="Contact Name (required)" required>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="contact_name">Billing Address Line 1</label>
                                            <input type="text" class="form-control" name="billing_address_line_1" id="billing_address_line_1" placeholder="Address Line 1">
                                        </div>
                                        
                                          <div class="form-group">
                                            <label for="contact_name">Billing Address Line 2</label>
                                            <input type="text" class="form-control" name="billing_address_line_2" id="billing_address_line_2" placeholder="Address Line 2">
                                        </div>
                                        
                                          <div class="form-group">
                                            <label for="contact_name">Billing City</label>
                                            <input type="text" class="form-control" name="billing_city" id="billing_city" placeholder="Billing City">
                                        </div>
                                        
                                          <div class="form-group">
                                            <label for="contact_name">Billing Pincode</label>
                                            <input type="text" class="form-control" name="billing_pincode" id="billing_pincode" placeholder="Billing Pincode">
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="delivery_address">Delivery Address</label>

                                            <input type="text" class="form-control" name="delivery_address" id="delivery_address" placeholder="Delivery Address">


                                        </div>
                                        <div class="form-group">
                                            <label for="delivery_address">Pin Code</label>

                                            <input type="text" class="form-control" name="deliveryAddPin" id="deliveryAddPin" placeholder="Enter Pincode" autocomplete="off">

                                        </div>







                                        <div class="form-group">
                                            <label for="pic_date">Pickup Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="pic_date" class="form-control pull-right" id="pic_date">
                                            </div>
                                        </div>

                                   
                                        <input type = "hidden" name = "unit_dur" value="Months">
                                        
                                         <div class="form-group">
                                            <label for="security_amt">Security Amount</label>

                                            <input type="text" class="form-control" name="security_amt" id="security_amt" placeholder="Security Amt" required>


                                        </div>

                                          

                                        </div>
                                        
                                    </div>
                                </div>
                                <hr>


                                <div class='row'>
                                    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                                        <table class="table table-bordered table-hover" id="table_auto">
                                            <thead>
                                                <tr>
                                                    <th width="2%"><input id="check_all" class="formcontrol" type="checkbox" /></th>
                                                    <th style="display:none;">Type</th>
                                                    <th>Item No</th>
                                                    <th>Description</th>
                                                    <th width="10%">Unit Price</th>
                                                    <th width="10%">Quantity</th>
                                                    <th width="10%">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>

                                                    <td><input class="case" type="checkbox" /></td>
                                                    <td style="display:none;"><select id="type_1" name="item_type[]" class="itemType"><option value="Item">Item</option><option value="Bundle">Bundle</option></select></td>
                                                    <td><input type="text" data-type="productCode" name="itemNo[]" id="itemNo_1" class="form-control autocomplete_txt" autocomplete="off"></td>
                                                    <td><input type="text" data-type="productName" name="itemName[]" id="itemName_1" class="form-control autocomplete_txt" autocomplete="off"></td>
                                                    <td><input type="number" name="price[]" id="price_1" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>


                                                    <td><input type="number" name="quantity[]" id="quantity_1" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
                                                    <td><input type="number" name="total[]" id="total_1" class="form-control totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class='row'>
                                    <div class='col-xs-12 col-sm-3 col-md-3 col-lg-3'>
                                        <button class="btn btn-danger delete" type="button">- Delete</button>
                                        <button class="btn btn-success addmore" type="button">+ Add More</button>
                                    </div>

                                    <div class="col-md-6" style="float:right;">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon"> Total: ₹</div>
                                                <input type="number" step="any" class="form-control" name="subTotal" id="subTotal" placeholder="Subtotal" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    Freight: ₹
                                                </div>
                                                <input type="number" step="any" class="form-control" name="freight" id="freight" placeholder="Freight" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    Sub Total: ₹
                                                </div>
                                                <input type="number" step="any" class="form-control" name="sub_total_freight" id="sub_total_freight" placeholder="SubTotal" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">Tax: ₹</div>
                                                <input type="number" step="any" class="form-control" id="tax" name="tax" placeholder="Tax" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" readonly>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">Total: ₹</div>
                                                <input type="number" step="any" class="form-control" name="totalAftertax" id="totalAftertax" placeholder="Total" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                    </div>

                    <hr>
                </div>
                <input type="hidden" value="Rental" name="quot_type">
                <div class="box-footer">
                    <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
            </div>
        </div>
</div>
</div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php include("includes/footer.php"); ?>

<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="dist/js/jquery-ui.min.js"></script>
<script src="dist/js/rentalquot.js"></script>

<script src="plugins/jQuery/jQuery-validate.js"></script>

<script type="text/javascript">
		$.validator.setDefaults( {
			submitHandler: function (form) {

        var formData = new FormData( form );

        console.log("going to submit");
        $.ajax({
           //url: window.location.pathname,
            url: 'secure/sec_quotation_add.php',
            type: 'POST',
            data: formData,
          // async: false,
            beforeSend: function() {
            document.getElementById("loader").style.display = "block";
              
            },
            
            success: function (data) {
              //console.log("success");
            //  var url = 'http:www.google.com';
             //   var url = parse_str(data);
              //  data = "viewquotation.php?id=89"
                 if( /^viewquotation.php\?id=([0-9])+/.test(data) ) window.open(data, "_self");
                 else alert("Error" + data);

              //  console.log(url);
              //  if(r.test(url)) console.log('passed');//
               
              //  else console.log('failed');//alert("Error");
               // location.reload(); 
            },
            error: function(data){
               console.log("error");
                console.log(data);
            },
            cache: false,
            contentType: false,
            processData: false
        }).complete(function() {
                document.getElementById("loader").style.display = "none";
            });

        return false;
//});
			}
		} );

		$( document ).ready( function () {
			$( "#myForm" ).validate( {
				rules: {
					name: "required",
					deliveryAddPin: {
                      minlength: 6,
                      digits: true,
                       maxlength: 6
                   },
                   site_name:{
                       maxlength: 100
                   },
                   contact_name: {
                       required: true,
                       maxlength: 50
                   },
                   billing_address_line_1: {
                       maxlength: 300
                   },
                   billing_address_line_2: {
                       maxlength: 300
                   },
                   billing_city: {
                       maxlength: 50
                   },
                   billing_pincode: {
                       minlength: 6,
                       digits: true,
                       maxlength: 6
                   },
                   delivery_address:{
                       maxlength: 300
                   },
                   security_amt:{
                       maxlength: 10,
                       digits: true
                   },
                   freight: {
                       required: true,
                       maxlength: 10
                   }
				},
				messages: {
					name: "Please enter Party Name"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
			} );

		
		} );
	</script>
