<?php include("includes/header.php"); ?>

<?php
 include("includes/dbcon.php");
include("secure/db_connect.php");
include("generate_challan_body.php");
$challan_total = 0;

$our_gst = '';
$job_order = $_GET['id'];
$challan_id = $_POST['challan_id'];
$challan_type = '';

$pickup_location_id = '';
$delivery_location_id = '';

$pickup_address = '';
$delivery_address = '';
$challan_amt = '';
$timestamp = '';
$remarks = '';


$query_loc_id = "SELECT * from table_challan WHERE challan_id = '$challan_id'";

$loc_ids = mysqli_query($con, $query_loc_id);
                  	while($locations=mysqli_fetch_array($loc_ids)){
                        
                        $pickup_location_id = $locations['pickup_loc_id'];
                        $delivery_location_id = $locations['delivery_loc_id']  ;   //JOB Order
                        $challan_type =  $locations['type'];
                        $challan_amt =  $locations['challan_tot'];
                        $timestamp = $locations['timestamp'];
                         $remarks = $locations['remarks'];
                    }


    $query_pickup_loc = "SELECT * from table_location WHERE location_id='$pickup_location_id''";

$pickup_location = mysqli_query($con, $query_pickup_loc);
    while($pick=mysqli_fetch_array($pickup_location)){
                        $pickup_address = $pick['address'].'<br>'.$pick['state']."<br>".$pick['pincode'];
                    }



if($challan_type == 1) {
     $q = "SELECT   qb_cache_customer.customer_name, table_quotation.delivery_address, table_quotation.delivery_pincode, table_quotation.contact_name, table_quotation.place_of_supply, state_codes.state, qb_cache_customer.billing_address, qb_cache_customer.billing_pincode, customer_local.gst_no FROM table_quotation , qb_cache_customer, state_codes, customer_local WHERE table_quotation.job_order = ? AND table_quotation.customer_id = qb_cache_customer.customer_id AND table_quotation.customer_id = customer_local.customer_id AND state_codes.state_num = table_quotation.place_of_supply";
    if ( $stmt = $mysqli->prepare( $q ) ) {
        $stmt->bind_param('s', $delivery_location_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($customer_name, $delivery_address, $delivery_pincode, $contact_name,  $place_of_supply, $state_of_supply, $billing_address, $billing_pincode, $gst);
        $stmt->fetch();
    }else echo $mysqli->error;
    

    
    
     $q = "SELECT  address, state, pincode FROM table_location WHERE location_id = ? ";
    if ( $stmt = $mysqli->prepare( $q ) ) {
        $stmt->bind_param('s', $pickup_location_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($address, $state, $pincode);
        $stmt->fetch();
    }else echo $mysqli->error;
    
    $pickup_address = $address."<br>". $state."<br>". $pincode;
    
    
}


$tot_items = $mysqli->prepare( "SELECT SUM(quantity) FROM `challan_item_relation` WHERE challan_id = ?" );
$tot_items->bind_param('s', $challan_id);
$tot_items->execute();
$tot_items->store_result();
$tot_items->bind_result($item_count);
$tot_items->fetch();


$challan_order_item_relation = $mysqli->prepare("SELECT c.item_code, c.qty, i.name FROM challan_order_item_relation AS c, table_item AS i WHERE challan_id = ? AND i.item_code = c.item_code");
$challan_order_item_relation->bind_param('s', $challan_id);
$challan_order_item_relation->execute();
$challan_order_item_relation->store_result();
$challan_order_item_relation->bind_result($item_code, $qty, $item_name);


  $pickup_state_code = '';
  switch($pickup_location_id) {
    case "Gnoida":
      $pickup_state_code = '09';
      $our_gst = '09AAACY4840M1ZV';
      break;
    case "Mumbai":
      $pickup_state_code = '27';
      $our_gst = '27AAACY4840M1ZX';
      break;
    case "Chennai":
      $pickup_state_code = '33';
      $our_gst = '33AAACY4840M1Z4';
      break;
    case "Kolkata":
      $pickup_state_code = '19';
      $our_gst = 'Not provided';
      break;
    case "Ahmedabad":
      $pickup_state_code = '24';
      $our_gst = '24AAACY4840M1Z3';
      break;
    case "Bangalore":
      $pickup_state_code = '29';
      $our_gst = '29AAACY4840M1ZT';
      break;
    default :
      $pickup_state_code = 'N/A';
      $our_gst = 'N/A';
      break;
  }
$interstate = false;
if($place_of_supply === $pickup_state_code ) $interstate = true;


$header = getHeader($interstate);
$body = getBody($interstate, $challan_id);
$footer = getFooter($interstate, $item_count, $challan_amt);
?>

<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;    
}

.noBorder {
    border:none !important;
}

.right {
    position: absolute;
    right: 0px;
    width: 300px;
    border: none;
    padding: 10px;
}
</style>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Challan
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Challan</a></li>
        <li class="active">View CHallan</li>
      </ol>
    </section>

    <div class="pad margin no-print">
      <div class="callout callout-info" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> Note:</h4>
        This page has been enhanced for printing. Click the print button at the bottom of the challan to print.
      </div>
    </div>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
              <img src="logo.png">
              
             <medium class="pull-right">
              <?php
          
          if($challan_type=='0'){
              echo "Sales Challan";
          }elseif($challan_type=='1'){
              echo "Delivery Challan";
;
          }elseif($challan_type=='2'){
              echo "Pickup Challan";
          }
          
          ?>
                 <small><strong><?php echo "<br>Dated: ".date('M j Y', strtotime($timestamp)); ?></strong></small>
              </medium>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
        <?php if($challan_type=='1') echo "<h5 style='text-align: center;'>Goods supplied under lease arrangement on returnable basis after completion of job (Not meant for sale)</h5>"; 
        ?>

        
        <hr>
 
      <!-- /.row -->
    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
        <table style="width:100%">
        <tr>
          <th>Youngman India Private Ltd</th>
          <th>Delivery Challan</th>
          <th>Original For Consignee</th>
        </tr>

        <tr>
          <td><?php echo $pickup_address;?><br><b>GSTIN: </b> <?php echo $our_gst; ?></td>
           <td><b>Challan No : </b><?php echo $challan_id; ?></td>
          <td><b>Date : </b><?php echo date('M j Y', strtotime($timestamp)); ?></td>
        </tr>

        <tr>
          <td><b>Consignee Name & Address</b></td>
           <td rowspan="6" colspan="2">Remarks:<br><?php echo $remarks; ?></td>
        </tr>
        <tr>
          <td><?php echo $delivery_address; ?></td>
        </tr>
       
      
        <tr>
          <td><b>Place of Supply: </b><?php echo  $state_of_supply; ?></td>
        </tr>
      </table>
</div>
</div>

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table style="width:100%">
          <thead>
          <?php echo $header; ?>
          </thead>
           
            <tbody>

                <?php echo $body; ?>
            
            </tbody>
             <?php echo $footer; ?>
          </table>
        </div>
        <!-- /.col -->
      </div>
        
      <!-- /.row -->
<div class="row" style="width:100%">
 <h5 style='text-align: center;'><strong> Using these items the following can be set up</strong></h5>
</div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table">
            <thead>
            <tr>
        
                <th>Item Id</th>
                <th>Qty</th>
                
            </tr>
            </thead>
            <tbody>
                <?php while($challan_order_item_relation->fetch()) {
                  echo "<tr><td>$item_name</td><td>$qty</td></tr>";
                } ?>
              
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>

 <div class="row" style='text-align: right; padding: 15px;  right: 10px;'>
        <div>
  <p><b>For Youngman India Pvt. Ltd.</b></p>

  <br><br><br>

  <p>Authorized signatory</p>
  </div>
</div>

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
        <a href="#" class="yourlink btn btn-default">Print</a>

       <!--   <a href="print_challan.php?id=<?php echo $_POST['challan_id']; ?>&job=<?php echo $job_order; ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> -->
            <?php if ($_SESSION['role']=="planning"): ?>
            <form method="post" action="quash_challan.php">
            <input type="hidden" name="challan_id" value="<?php echo $challan_id; ?>">           
            <button type="submit" class="btn btn-danger pull-right" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Quash Challan
            </button>
            </form>
             <?php endif; ?>

        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
 <?php include("includes/footer.php"); ?>

 <script>
  $('a.yourlink').click(function(e) {
    e.preventDefault();
    window.open("print_challan.php?id=<?php echo $_POST['challan_id']; ?>&job=<?php echo $job_order; ?>");
    window.open("print_duplicate.php?id=<?php echo $_POST['challan_id']; ?>&job=<?php echo $job_order; ?>");
     window.open("print_triplicate.php?id=<?php echo $_POST['challan_id']; ?>&job=<?php echo $job_order; ?>");
});
 </script>