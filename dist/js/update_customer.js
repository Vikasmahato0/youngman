 $(document).ready(function() {
     $("#name").keyup(function() {
         $.ajax({
             type: "POST",
             url: "ajax/readCustomerFromCache.php",
             data: 'keyword=' + $(this).val(),
             beforeSend: function() {
                 $("#name").css("background", "#FFF url(img/ LoaderIcon.gif) no-repeat 165px");
             },
             success: function(data) {
                 $("#suggesstion-box").show();
                 $("#suggesstion-box").html(data);
                 $("#name").css("background", "#FFF");
                 console.log("data");
             }
         });
     });
 });

 function selectCustomer(id, name, mailingAddress, mailingPincode, creditLimit, creditTerm) {
     console.log(name);
     $("#id").val(id);
     $("#suggesstion-box").hide();
     $("#name").val(name);
     $('#mailAddLine').val(mailingAddress);
     $('#mailAddPin').val(mailingPincode);
     $('#creditLimit').val(creditLimit);
     $('#creditTerm').val(creditTerm);
 }