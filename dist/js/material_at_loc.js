    $('.check').change(function() {
        var data = $(this).val();
        data = encodeURI(data).replace(/&/g, "%26");
        $.ajax({
            "type": "POST",
            "url": "ajax/get_material.php",
            data: 'location=' + data,
            "success": function(data) {
                $("#location_details")
                    .html(data);
            }
        });


    });

    $(document).ready(function() {
        $("#job").keyup(function() {
            var data = $(this).val();
            data = encodeURI(data).replace(/&/g, "%26");
            $.ajax({
                type: "POST",
                url: "ajax/get_jobs.php",
                data: 'keyword=' + data,
                beforeSend: function() {
                    $("#job").css("background", "#FFF url(img/ LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data) {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#name").css("background", "#FFF");
                    console.log("data");
                }
            });
        });
    });

    function selectJob(job) {
        job = encodeURI(job).replace(/&/g, "%26");
        console.log(job);
        $("#suggesstion-box").hide();
        $("#job").val(job);
        $.ajax({
            "type": "POST",
            "url": "ajax/get_material.php",
            data: 'location=' + job,
            "success": function(data) {
                $("#location_details")
                    .html(data);
            }
        });
    }