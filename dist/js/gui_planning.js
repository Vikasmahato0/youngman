var from_location = $("#from_location").text();

function verifyUser(password) {
    if (password.value == "abcd") addEmptyRow();
    else alert("Wrong password");
}


$(".addmore").on('click', function () {
    $('#verify').modal('show');
});

var length = $('#table_challan_rental').length;

$(document).on('change keyup blur', '.search', function () {
    var keyword = $(this).val();
    var type = '';
    id_arr = $(this).attr('id');
    id = id_arr.split("_");
    if(id[0]=="itemNo") { type = "item_no";}
    if(id[0]=="itemName") { type = "item_name";}
    
    //$('#desc_'+id[1]).val(desc[itemCode]);  
   
      $.ajax({
            type: "POST",
            url: "ajax/getItemCustomPlanning.php",
            data: {
                keyword: keyword,
                type: type
            },

            success: function (data) {
                console.log(data);
              //  avail_qty = (+data); //because typeof (+data) is int
            //    addRow(ID, desc, price, 1, avail_qty);
                calculateTotal()
            }
        });
});

function addEmptyRow() {

    //   length = length + 1;
    //  alert(length);
    var html = "<tr id='' class = 'item'> <td><input class='case' type='checkbox'/></td>" +
        "  <td> <input type='text' data-type='productCode' name='itemNo[]' id='itemNo_" + length + "' class='form-control autocomplete_txt search' autocomplete='off' value='' readonly></td>" +
        "<td> <input type='text' data-type='productName' name='itemName[]' id='itemName_" + length + "' class='form-control autocomplete_txt search' autocomplete='off' value=''></td>" +
        "  <td><input type='number' name='price[]' id='price_" + length + "' class='form-control changesNo price' autocomplete='off' onkeypress='return IsNumeric(event);' ondrop='return false;' onpaste='return false;' value=''></td>" +
        "<td><input type='number' name='quantity[]' id='quantity_" + length + "' class='form-control changesNo quantity' autocomplete='off' onkeypress='return IsNumeric(event); ondrop='return false;' onpaste='return false;' value='1'></td>" +
        "<td><input type='number' name='quantity_aval[]' id='quantity_aval_" + length + "' class='form-control changesNo quantity' autocomplete='off' onkeypress='return IsNumeric(event); ondrop='return false;' onpaste='return false;' value='1' readonly></td>" +
        " <td><input type='number' name='total[]' id='total_" + length + "' class='form-control totalLinePrice' autocomplete='off' onkeypress='return IsNumeric(event);'ondrop='return false;' onpaste='return false;' value='' readonly></td>" + "</tr>";

    $("#table_challan_rental tbody").append(html);

    length++;
}

$("#required_items").on('click', '.btnSelect', function () {
    // alert("clicked");

    var currentRow = $(this).closest("tr");
    var ID = currentRow.find("td:eq(1)").text();
    var bundle = currentRow.find("td:eq(2)").text();
    var qty = currentRow.find("td:eq(3)").text();
    var desc = currentRow.find("td:eq(4)").text();
    var price = currentRow.find("td:eq(5)").text();

    // alert(desc);
    qty = parseInt(qty, 10);
    if (qty > 0) {
        currentRow.find("td:eq(3)").text(qty - 1);

        var row_id = $("#fullfilled_items").find('#' + ID).html();
        //   alert(row_id);
        if (row_id === undefined || row_id === null) {
            console.log("addd to bom");
            var new_row = "<tr id='" + ID + "'><td></td><td><input type='text' name='itemCodeBOM[]' value='" + ID + "' readonly></td><td><input type='text' name='itemQtyBOM[]' value='1' readonly></td></tr>";
            $("#fullfilled_items tbody").append(new_row);
        } else {
            console.log("already exists");
            var oldQty = $("#fullfilled_items").find('#' + ID).find("td:eq(2)").find('input').val();

            var newQty = parseInt(oldQty) + 1;
            //  alert(ID + " " +oldQty);

            $("#fullfilled_items").find('#' + ID).find("td:eq(2)").find('input').val(newQty);

        }

    } else return;

    if (bundle == "Bundle") {
        $.ajax({
            type: "POST",
            url: "expandBundle.php",
            data: {
                keyword: ID,
                location_id: from_location
            },

            success: function (data) {

                console.log(data);

                var obj = JSON.parse(data);
                console.log(obj);

                for (var prop in obj) {
                    addRow(obj[prop].item_code, obj[prop].description, obj[prop].value, obj[prop].quantity, obj[prop].avail_qty);

                }
                calculateTotal()
            }
        });



    } else {
        var avail_qty = '';
        $.ajax({
            type: "POST",
            url: "getAvailItemQty.php",
            data: {
                keyword: ID,
                location_id: from_location
            },

            success: function (data) {
                console.log(data);
                avail_qty = (+data); //because typeof (+data) is int
                addRow(ID, desc, price, 1, avail_qty);
                calculateTotal()
            }
        });
    }
});

$(document).on('change keyup blur', '.changesNo', function () {
    var parent_id = $(this).closest('tr').attr('id');
    var qty = $("#table_challan_rental").find('#' + parent_id).find("td:eq(4)").find('input').val();

    var price = $("#table_challan_rental").find('#' + parent_id).find("td:eq(3)").find('input').val();
    qty = parseInt(qty);
    price = parseFloat(price).toFixed(2);
    var new_price = qty * price;
    $("#table_challan_rental").find('#' + parent_id).find("td:eq(6)").find('input').val(new_price);
    calculateTotal();
});

function calculateTotal() {
    subTotal = 0;
    $('.totalLinePrice').each(function () {
        if ($(this).val() != '') subTotal += parseFloat($(this).val());
    });
    $('#subTotal').val(subTotal.toFixed(2));
}

function addRow(ID, desc, price, quantity, avail_qty) {
    console.log(ID + desc + price + quantity);
    var ids = $("#table_challan_rental").find('#' + ID).html();
    var totLinePrice = parseFloat(price * quantity).toFixed(2);
    if (ids === undefined || ids === null) {



        var html = "<tr id='" + ID + "' class = 'item'> <td><input class='case' type='checkbox'/></td>" +
            "  <td> <input type='text' data-type='productCode' name='itemNo[]' id='itemNo_0' class='form-control autocomplete_txt' autocomplete='off' value='" + ID + "' readonly></td>" +
            "<td> <input type='text' data-type='productName' name='itemName[]' id='itemName_0' class='form-control autocomplete_txt' autocomplete='off' value='" + desc + "'></td>" +
            "  <td><input type='number' name='price[]' id='price_0' class='form-control changesNo' autocomplete='off' onkeypress='return IsNumeric(event);' ondrop='return false;' onpaste='return false;' value='" + price + "'></td>" +
            "<td><input type='number' name='quantity[]' id='quantity_0' class='form-control changesNo' autocomplete='off' onkeypress='return IsNumeric(event); ondrop='return false;' onpaste='return false;' value='" + quantity + "'></td>" +
            "<td><input type='number' class='form-control quantity_aval' autocomplete='off' onkeypress='return IsNumeric(event); ondrop='return false;' onpaste='return false;' value='" + avail_qty + "' readonly></td>" +
            " <td><input type='number' name='total[]' id='total_0' class='form-control totalLinePrice' autocomplete='off' onkeypress='return IsNumeric(event);'ondrop='return false;' onpaste='return false;' value='" + totLinePrice + "' readonly></td>" + "</tr>";

        $("#table_challan_rental tbody").append(html);
    } else {
        var oldQty = $("#table_challan_rental").find('#' + ID).find("td:eq(4)").find('input').val();
        var newQty = parseInt(oldQty) + parseInt(quantity);
        $("#table_challan_rental").find('#' + ID).find("td:eq(4)").find('input').val(newQty);
        var total = newQty * parseFloat(price).toFixed(2);
        $("#table_challan_rental").find('#' + ID).find("td:eq(6)").find('input').val(total);

    }
}