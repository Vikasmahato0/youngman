 $(document).ready(function() {
            $("#name").keyup(function() {
                $.ajax({
                    type: "POST",
                    url: "ajax/readCustomerFromCache.php",
                    data: 'keyword=' + $(this).val(),
                    beforeSend: function() {
                        $("#name").css("background", "#FFF url(img/ LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function(data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#name").css("background", "#FFF");
                        console.log("data");
                    }
                });
            });
        });

        function selectCustomer(id, name, billingAddressLine1, billingAddressLine2, billingAddressLine3, billingPincode) {
            console.log(name);
            $("#id").val(id);
            $("#suggesstion-box").hide();
            $("#name").val(name);
            $('#billing_address_line_1').val(billingAddressLine1);
            $('#billing_address_line_2').val(billingAddressLine2);
            $('#billing_city').val(billingAddressLine3);
            $('#billing_pincode').val(billingPincode);
        }