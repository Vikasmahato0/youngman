<?php include("includes/header.php"); ?>
<?php 
$id = $_GET['id'];
$query = "SELECT
          job_order, delivery_address, delivery_pincode, customer_name, delivery_date, work_order_image, security_letter_image, rental_payment_image, security_neg_image
          FROM
          table_quotation
          WHERE
          s_no = ?";

if($stmt = $mysqli->prepare($query)){
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($job_order, $delivery_address, $delivery_pincode, $customer_name, $delivery_date, $work_order_image, $security_letter_image, $rental_payment_image, $security_neg_image);
        $stmt->fetch();
}else echo $mysqli->error;


?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Orders
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Orders</a></li>
        <li class="active">View</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- title row -->
      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
	          <h3 class="box-title">
	            Order Number <?php echo $_GET['id']; ?>
	          </h3>
        	</div>
        <!-- /.col -->
            <div class="box-body">
                <blockquote>
	          <strong>Order No:
	            </strong><?php echo $job_order; ?>
	          </blockquote>
                
              
                 <blockquote>
	          <strong>Delivery Address:
	            </strong><?php echo $delivery_address.'<br>'.$delivery_pincode; ?>
	          </blockquote>
                 <blockquote>
	          <strong>Name:
	            </strong><?php echo $customer_name; ?>
	          </blockquote>
             
                <blockquote>
	          <strong>Delivery Date:
	            </strong><?php echo $delivery_date; ?>
	          </blockquote>
                
                
        	</div>

		    </div>
		  </div>

        <div class="col-md-6">

        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#work_order">Work Order</a></li>
          <li><a data-toggle="tab" href="#security_letter">Security Letter</a></li>
          <li><a data-toggle="tab" href="#rental_payment">Rental Payment</a></li>
          <li><a data-toggle="tab" href="#security_negotiable">Security Negotiable</a></li>
        </ul>

        <div class="tab-content">
          <div class="box box-default tab-pane fade in active" id="work_order">
            <div class="box-header with-border">
              <h3 class="box-title">
                Work Order
              </h3>
            </div>
            <!--<img src = "<?php echo $work_order_image; ?>" style="width:94%; margin: 3%;" >-->
               <a href="<?php echo $work_order_image; ?>" target="_blank"> Work Order</a>
          </div>
          <div class="box box-default tab-pane fade in" id="security_letter">
            <div class="box-header with-border">
              <h3 class="box-title">
                Security Letter
              </h3>
        	  </div>
        	  <!--<img src = "<?php echo $security_letter_image; ?>" style="width:94%; margin: 3%;" >-->
              <a href="<?php echo $security_letter_image; ?>" target="_blank"> Security Letter</a>
          </div>
          <div class="box box-default tab-pane fade in" id="rental_payment">
            <div class="box-header with-border">
              <h3 class="box-title">
                Rental Payment
              </h3>
            </div>
          <!--  <img src = "<?php echo $rental_payment_image; ?>" style="width:94%; margin: 3%;" >-->
              <a href="<?php echo $rental_payment_image; ?>" target="_blank">Rental Payment</a>
          </div>
          <div class="box box-default tab-pane fade in" id="security_negotiable">
            <div class="box-header with-border">
              <h3 class="box-title">
                Security Negotiable
              </h3>
        	  </div>
        	  <!--<img src = "<?php echo $security_neg_image; ?>" style="width:94%; margin: 3%;" >-->
              <a href="<?php echo $security_neg_image; ?>" target="_blank"> Security Negotiable</a>
		      </div>
        </div>
		  </div>
       
		</div>
    </section>
    <!-- /.content -->

    <div class="clearfix"></div>
</div>

<?php include("includes/footer.php"); ?>