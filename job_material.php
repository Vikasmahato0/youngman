<?php include ("includes/header.php");?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
<link href="dist/css/jquery-ui.min.css" rel="stylesheet">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Items at Job Order
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Job Order</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <form action="movement_analysis.php" method="post">
        <div class="input-group input-group-sm col-sm-4">
  

                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" autocomplete="off" required>

                                        </div>
                                         <div id="suggesstion-box"></div>

                                        <div class="form-group">
                                             <select class="form-control job col-sm-4" name="job_order" id="job_for_cust">        
                                                <option>Select</option>
                                            </select>

                                        </div>
                                    
                                       
        <button type="submit" >Get Movement Analysis</button>
              </div>
            
            </form>

        <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Items</h3>
            </div>
            <!-- /.box-header -->
              <div id="location_details" class="box-body table-responsive no-padding">
            
         
                  
              </div>
              
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
        
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
<!--
<script src="dist/js/material_at_loc.js"></script> 
-->
<script>
  
 $(document).ready(function() {
            $("#name").keyup(function() {
                $.ajax({
                    type: "POST",
                    url: "ajax/readCustomerFromCache.php",
                    data: 'keyword=' + $(this).val(),
                    beforeSend: function() {
                        $("#name").css("background", "#FFF url(img/ LoaderIcon.gif) no-repeat 165px");
                        //console.log($(this).val());
                    },
                    success: function(data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#name").css("background", "#FFF");
                        console.log("data");
                    }
                });
            });
        });

        function selectCustomer(id, name) {
            console.log(name);
            console.log(id);
            $("#id").val(id);
            $("#suggesstion-box").hide();
            $("#name").val(name);
          $.ajax({
                    type: "POST",
                    url: "ajax/get_job_for_customer.php",
                    data: {keyword: id
                            },
                    beforeSend: function() {
                       
                    },
                    success: function(response) {
                     
                     $("#job_for_cust").empty();
                     $("#job_for_cust").append(response);
                     addSelectChange();
                    }
                });
        }


        function addSelectChange() {
                        $('select').bind('change', function() {
                            var data = $(this).val();
        data = encodeURI(data).replace(/&/g, "%26");
        $.ajax({
            "type": "POST",
            "url": "ajax/get_material.php",
            data: 'location=' + data,
            "success": function(data) {
                $("#location_details")
                    .html(data);
            }
        });
                        });
                      } 
</script>