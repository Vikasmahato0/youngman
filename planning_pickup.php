<?php include ("includes/header.php");?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pickup
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pickup</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        <!-- Modal -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Pickup CHallans</h3>
            </div>
            <!-- /.box-header -->
            
                
            <div class="box-body">
              <table id="godown" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Challan ID</th>
                    <th>Job Order</th>
                    <th>Warehouse</th>
                    <th>Recieving Dt</th>
                    <th>GR No</th>
                    <th>Amt</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                    $q = "SELECT challan_id, pickup_loc_id, delivery_loc_id, recieving_date, gr_no, amt FROM table_challan WHERE ( type=2 AND recieving_date  IS NOT NULL ) AND status = 1 ";
                    
                    if($stmt = $mysqli->prepare($q)){
                          $stmt->execute();
                    $stmt->store_result();
                    $stmt->bind_result($challan_id, $pickup_loc_id, $delivery_loc_id, $recieving_date, $gr_no, $amt );
                    }
                    while($stmt->fetch()){
                    ?>
                    <tr>
                    <td><?php echo $challan_id; ?></td>
                    <td><?php echo $pickup_loc_id; ?></td>
                    <td><?php echo $delivery_loc_id; ?></td>
                    <td><?php echo $recieving_date; ?></td>
                    <td><?php echo $gr_no; ?></td>
                    <td><?php echo $amt; ?></td>
                    <td><form action="pickupChallan.php" method="post">
                        <input type="hidden" name="csrf" value="<?php echo $_SESSION['login_string']; ?>" >
                        <input type="hidden" value="<?php echo $challan_id; ?>" name="challan_id">
                        <input type="hidden" value="<?php echo $pickup_loc_id; ?>" name="job_order">
                         <button class="btn btn-success" type="submit" name="pickup_btn" id="pickup_btn">Pickup</button> 
                        </form></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Challan ID</th>
                    <th>Job Order</th>
                    <th>Warehouse</th>
                    <th>Recieving Dt</th>
                    <th>GR No</th>
                    <th>Amt</th>
                    <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
      
           
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
<script>
setTimeout(function(){
   window.location.reload(1);
}, 60000);
</script>