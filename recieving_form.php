
<?php include ("includes/header.php");
$job_order = $_POST['job_order'];
$challan_id = $_POST['challan_id'];
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div id="loader" style="display:none;"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Recieving Form
        <small>Provide complete details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Add Recieving</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
            <form action="add_recieving.php" method="post" id="myForm" enctype="multipart/form-data">
                <input type="hidden" name="jobOrder" id="JobOrder" value="<?php echo $job_order; ?>" >
                <input type="hidden" name="bookId" value="<?php echo $challan_id; ?>" >
                <input type="hidden" name="warehouse_id" value="<?php echo $_SESSION['username']; ?>" >
        <div class="box-header with-border">
          <h3 class="box-title">Recieving Form</h3>
        </div>
        <div class="box-body">
           <div class='row'>
                                    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                                        <table class="table table-bordered table-hover" id="table_recieving">
                                            <thead>
                                                <tr>
                                                    <th width="2%"><input id="check_all" type="checkbox" /></th>
                                                    <th width="30%">Item ID</th>
                                                    <th width="34%">Description</th>
                                                    <th width="10%">OK Qty</th>
                                                    <th width="10%">Missing</th>
                                                    <th width="10%">Damage</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
            
            <button class="btn btn-danger deleterow" type="button">- Delete</button>
                                        <button class="btn btn-success addnewrow" type="button">+ Add More</button>
        </div>
        <!-- /.box-body -->
          
        <div class="box-footer">
           <div class="form-group">
                                            <label for="fileToUpload">Recieving</label>
             <input type="file" name="fileToUpload" id="fileToUpload" accept=".pdf,.jpg, .jpeg, .png, .gif" required="true">
            </div>

                              <!--          <div class="form-group">
                                            <label for="transporter">Transporter</label>
                                            <input class="form-control" type="text" name="transporter" id="transporter" placeholder="Transporter Name" required="true">

                                        </div> -->

                                            <div class="form-group">
                                            <label>Select Transporter</label>
                                            <select class="form-control" name="transporter" id="transporter" required="true">
                                                
                                                <option>Select</option>
                                                
                                                <?php 
                                                $vendors = "SELECT vendor_id, vendor_name FROM table_vendors";
                                                
                                                if($transporters = $mysqli->prepare( $vendors )){
                                                    $transporters ->execute();
                                                    $transporters ->store_result();
                                                    $transporters ->bind_result($vendor_id, $vendor_name);   

                                                    }else echo $mysqli->error;

                                                while( $transporters->fetch()){
                                                    
                                                    echo '<option value="'.$vendor_id.'">'.$vendor_name.'</option>';
                                                }
                                                
                                                ?>
                                                
                                                
                                            </select>
                                        </div>


                                        <div class="form-group">
                                            <label for="name">Recieving Date</label>
                                            
                                            
                                             <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="recieving_date" class="form-control pull-right" id="del_date">
                                            </div>
                                            
                                            
                                        </div>

                                        <div class="form-group">
                                            <label for="gr_no">LR No</label>
                                            <input class="form-control" type="text" name="gr_no" id="gr_no" placeholder="GR Number" required="true">

                                        </div>
                                        <div class="form-group">
                                            <label for="amt">Transport Amount</label>
                                            <input class="form-control" type="text" name="amt" id="amt" placeholder="Transport Amount" required="true">

                                        </div>
              <div class="form-group">
                
                  <textarea rows="7" cols="100" placeholder="Add remarks here" name="remarks" id="remarks" ></textarea>
            </div>
           <button type="submit" name="recieving_submit" id="submit" class="btn btn-primary">Submit</button>
            
        </div>
                                   
                                        
                                    
          </form>
        <!-- /.box-footer-->
                 
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
<!--<script src="dist/js/recieving_form.js"></script>-->
        <script>
            var error = false;
            
        var job_order =$('#JobOrder').val();
        job_order = encodeURI(job_order).replace(/&/g, "%26");
   // alert(job_order);
        var options = '<option>Select</option>';
        var itemcode = {};
        var quantity = {};
 
         $.ajax({
            "type":"POST",
            "url":"ajax/get_code.php",
            data:'location='+job_order,
            "success":function(data){
             var obj = JSON.parse(data);
        
            for (var prop in obj) 
            { 
                options += "<option value = '"+obj[prop].item_code+"' >"+obj[prop].description+"</option>"; 
                itemcode[obj[prop].item_code] = obj[prop].item_code;
                quantity[obj[prop].item_code] = obj[prop].quantity;
            }
               console.log("itemcode" + itemcode);
            }
        });
        var i=1;
        $(".addnewrow").on('click',function(){
        
         console.log(options);
            
            var html = ' <tr id = "rowid_'+i+'">'
            +'<td><input class="case" type="checkbox" /></td>'
            +'<td><input type="text" class="form-control select_code" id="selectcode_'+i+'" name="code[]" required></td>'
            +'<td><select class="form-control desc" id="desc_'+i+'" name="desc[]">'+options+'</select></td>'
           
            +'<td><input type="text" class="form-control ok_qty changesNo" name="ok_qty[]" id="okqty_'+i+'" value="0" required></td>'
            +'<td><input type="text" class="form-control missing_qty changesNo" name="missing_qty[]" id="missingqty_'+i+'" value="0" required></td>'
            +'<td><input type="text" class="form-control damage_qty changesNo" name="damage_qty[]" id="damageqty_'+i+'" value="0" required></td></tr>';
            
            $('#table_recieving').append(html);
            i++;
	
    });
        
        $(".deleterow").on('click', function() {
	$('.case:checkbox:checked').parents("tr").remove();
	$('#check_all').prop("checked", false); 
});
        
            //to check all checkboxes
$(document).on('change','#check_all',function(){
	$('input[class=case]:checkbox').prop("checked", $(this).is(':checked'));
});
   
    $(document).on('change', '.desc', function(){
        var itemCode = $(this).val();
        id_arr = $(this).attr('id');
	  	id = id_arr.split("_");
	  	$('#selectcode_'+id[1]).val(itemcode[itemCode]);
        $(this).find('[value="+itemcode[itemCode]+"]').remove();
        
    });
            $(document).on('change', '.changesNo', function(){
                total = 0;
                id_arr = $(this).attr('id');
	            id = id_arr.split("_");
                itemCode = $('#selectcode_'+id[1]).val();
	            ok_qty = $('#okqty_'+id[1]).val();
	            missing_qty = $('#missingqty_'+id[1]).val();
                damage_qty = $('#damageqty_'+id[1]).val();
                
                if(ok_qty == '') $('#okqty_'+id[1]).val(0);
                if(missing_qty == '') $('#missingqty_'+id[1]).val(0);
                if(damage_qty == '') $('#damageqty_'+id[1]).val(0);
                
                if(ok_qty != '' && missing_qty != '' && damage_qty != '') {
                    total = parseInt(ok_qty) + parseInt(missing_qty) + parseInt(damage_qty);
                    console.log("total " + total);
                }
                
                if (total > parseInt(quantity[itemCode])){
                   document.getElementById('okqty_'+id[1]).style.borderColor = "red";
                    document.getElementById('missingqty_'+id[1]).style.borderColor = "red";
                    document.getElementById('damageqty_'+id[1]).style.borderColor = "red";
                    error = true;
                }
                
                if (total <= parseInt(quantity[itemCode])){
                     document.getElementById('okqty_'+id[1]).style.borderColor = "green";
                    document.getElementById('missingqty_'+id[1]).style.borderColor = "green";
                    document.getElementById('damageqty_'+id[1]).style.borderColor = "green";
                    error = false;
                }
  
            });
            
  
           
        </script>

        <script src="plugins/jQuery/jQuery-validate.js"></script>

<script type="text/javascript">
		$.validator.setDefaults( {
			submitHandler: function (form) {

                if(error) return;
                    var formData = new FormData( form );
                //action="add_recieving.php"
                    console.log(formData);
                    $.ajax({
                    // url: window.location.pathname,
                        url: 'add_recieving.php',
                        type: 'POST',
                        data: formData,
                    // async: false,
                        beforeSend: function() {
                        document.getElementById("loader").style.display = "block";
                            $('#my_modal').modal('hide');
                        },
                        
                        success: function (data) {
                            console.log(data);
                            window.open('dashboard_movement.php', '_self'); 
                        },
                        error: function(data){
                            console.log(data);
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    }).complete(function() {
                            document.getElementById("loader").style.display = "none";
                        });

                    return false;

     
			}
		} );

		$( document ).ready( function () {
			$( "#myForm" ).validate( {
				rules: {
					
                    transporter: "required",
                    recieving_date: "required",
                    gr_no: {
                      required: true,
                       maxlength: 10
                   },
                   amt: {
                       required: true,
                       maxlength: 10,
                       digits: true
                   }
				},
				messages: {
					transporter: "Please enter Transporter Name",
                    recieving_date: "Please enter Date of Recieving",
                    gr_no: "Please enter GR No",
                    amt: "Please enter Amount"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
			} );

		
		} );
	</script>