<?php
include 'secure/db_connect.php';
include 'secure/functions.php';

$page = basename($_SERVER['PHP_SELF']);
$getpage = explode('.',$page);
$currentPage = $getpage[0];
sec_session_start(); // Our custom secure way of starting a php session.

if( ! authorize($currentPage)){ header("Location: secure_login.php"); }

?>

<!DOCTYPE html>
    <html>
    <head>
         <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Youngman | Dashboard</title>
     <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="plugins/select2/select2.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
      <link href="dist/css/jquery-ui.min.css" rel="stylesheet">
  <style>
    .alert {
      position: fixed;
      top: 10%;
      bottom: auto;
      right: 10%;
      left: auto;
      display: block;
      z-index: 1;
    }
    .modal-body .form-horizontal .col-sm-2,
      .modal-body .form-horizontal .col-sm-10 {
      width: 100%
    }

    .modal-body .form-horizontal .control-label {
      text-align: left;
    }
    .modal-body .form-horizontal .col-sm-offset-2 {
      margin-left: 15px;
    }
    input[type='number'] {
        -moz-appearance:textfield;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }
  </style>

  <style>
	.ui-autocomplete {
		max-height: 200px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
	}
	/* IE 6 doesn't support max-height
	 * we use height instead, but this forces the menu to always be this tall
	 */
	* html .ui-autocomplete {
		height: 200px;
	}
	</style>
        <style>
#customer_list {
    /* Remove default list styling */
    list-style-type: none;
    padding: 0;
    margin: 0;
}

#customer_list li {
    border: 1px solid #ddd; /* Add a border to all links */
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6; /* Grey background color */
    padding: 5px; /* Add some padding */
    text-decoration: none; /* Remove default text underline */
    font-size: 14px; /* Increase the font-size */
    color: black; /* Add a black text color */
    display: block; /* Make it into a block element to fill the whole list */
}

#customer_list li:hover:not(.header) {
    background-color: #eee; /* Add a hover effect to all links, except for headers */
}
</style>

  </head>
  <body class="hold-transition skin-yellow sidebar-mini">
<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
      <a class="logo" href="#">
      <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>YM</b></span>
      <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Youngman</b></span>
      </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
    </nav>
  </header>


    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>

    <script src="dist/js/jquery-ui.min.js"></script>
    <style>
/* Center the loader */
#loader {
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 1;
  width: 150px;
  height: 150px;
  margin: -75px 0 0 -75px;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Add animation to "page content" */
.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 } 
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom { 
  from{ bottom:-100px; opacity:0 } 
  to{ bottom:0; opacity:1 }
}

</style>

  <?php include 'nav.php'; ?>
    