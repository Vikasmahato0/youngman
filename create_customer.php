

  <?php  include 'includes/header.php';
            if(login_check($mysqli) == true) { ?>

     <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div id="loader" style="display:none;"></div>

    <section class="content-header">
      <h1>
        New Customer
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">New Customer</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <form class="form-horizontal" id="newCustomer" action="" method="post">
           <input type="hidden" name="csrf" value="<?php echo $_SESSION['login_string']; ?>" >
           <div class="row">
               <div class="col-xs-6 col-sm-6 col-lg-6 "> 
                   <div class="box box-primary">
                       <div class="form-group">
                    
                  <label for="inputName" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10 row" id="inputName">
                         <input class="form-control col-sm-3" id="firstName" name="firstName" placeholder="First Name" type="text" required>
                         <input class="form-control col-sm-3" id="lastName" name="lastName" placeholder="Last Name" type="text" required>
                  </div>
                </div>
                       
                        <div class="form-group">
                  <label for="inputCompany" class="col-sm-2 control-label">Company</label>

                  <div class="col-sm-10 row">
                    <input class="form-control" id="inputCompany" name="company" placeholder="Company" type="text" required>
                  </div>
                </div>
                       
                       
                        <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10 row">
                    <input class="form-control" id="inputEmail" name="email" placeholder="Email" type="email" required>
                  </div>
                </div>
                    
                   <div class="form-group">
                  <label for="inputPhone" class="col-sm-2 control-label" >Phone</label>

                  <div class="col-sm-10 row">
                    <input class="form-control" id="inputPhone" placeholder="Primary Phone" name="primPhone" type="number" required>
                  </div>
                </div>
                  
                       <div class="form-group">
                  <label for="inputAltPhone" class="col-sm-2 control-label">Alternate Phone</label>

                  <div class="col-sm-10 row">
                    <input class="form-control" id="inputAltPhone" placeholder="Alternate Phone (optional)" name="altPhone" type="number">
                  </div>
                </div>
                         <div class="form-group">
                  <label for="inputMobile" class="col-sm-2 control-label">Mobile</label>

                  <div class="col-sm-10 row">
                    <input class="form-control" id="inputMobile" placeholder="Mobile (optional)" name="mobile" type="number">
                  </div>
                </div>
                                           <div class="form-group">
                  <label for="creditLimit" class="col-sm-2 control-label">Credit Limit</label>

                  <div class="col-sm-10 row">
                    <input class="form-control" id="creditLimit" placeholder="Credit Limit" name="creditLimit" type="text" required>
                  </div>
                </div>
                  
                    <div class="form-group">
                  <label for="creditTerm" class="col-sm-2 control-label">Credit Term</label>

                  <div class="col-sm-10 row">
                    <input class="form-control" id="creditTerm" placeholder="Credit Term" name="creditTerm" type="text" required>
                  </div>
                </div>
                       
                   </div>
               </div>
               
               
               
               <div class="col-xs-6 col-sm-6 col-lg-6"> 
                   <div class="box box-primary">
                     <div class="form-group">
                  <label for="inputBillAddress" class="col-sm-2 control-label">Billing Address</label>
                  <div class="col-sm-10 row" id="inputBillAddress">
                    <input class="form-control col-sm-3" id="billAddLine" name="billAddLine" placeholder="Line 1" type="text" required>
                        <input class="form-control col-sm-3" id="billAddLine2" name="billAddLine2" placeholder="Line 2" type="text">
                         <input class="form-control col-sm-3" id="billAddCity" name="billAddCity" placeholder="City" type="text" required>
                         <input class="form-control col-sm-3" id="billAddPin" name="billAddPin" placeholder="Pincode" type="number" required>
                  </div>
                </div>
                  
                  <div class="form-group">
                  <label for="inputMailAddress" class="col-sm-2 control-label">Mailing Address</label>
                  <div class="col-sm-10 row" id="inputMailAddress">
                    <input class="form-control col-sm-3" id="mailAddLine" name="mailAddLine" placeholder="Line 1" type="text" required>
                       <input class="form-control col-sm-3" id="mailAddLine2" name="mailAddLine2" placeholder="Line 2" type="text">
                         <input class="form-control col-sm-3" id="mailAddCity" name="mailAddCity" placeholder="City" type="text" required>
                         <input class="form-control col-sm-3" id="mailAddPin" name="mailAddPin" placeholder="Pincode" type="number" required>
                  </div>
                </div>
                         <div class="form-group">
                  <label for="inputGST" class="col-sm-2 control-label">GSTN</label>

                  <div class="col-sm-10 row">
                    <input class="form-control" id="gst" placeholder="GSTN" name="gst" type="text" required>
                  </div>
                </div>
                  
                    <div class="form-group">
                  <label for="cin" class="col-sm-2 control-label">CIN</label>

                  <div class="col-sm-10 row">
                    <input class="form-control" id="cin" placeholder="CIN" name="cin" type="text" required>
                  </div>
                </div>

                <input type="hidden" name = "category" value="Post" >
                       
                   </div>
               </div>
</div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h2>Please tick the documents which will be required when creating order</h2>
       <div class="form-group">
                       <div class="col-sm-offset-2 col-sm-10">
                           
                           <div class="form-group">
                  <label>Security Letter</label>
                  <select class="form-control" name="securityLetter">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                </div>
                           
                           <div class="form-group">
                  <label>Rental Advance</label>
                  <select class="form-control" name="rentalAdvance">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                </div>
                           
                           <div class="form-group">
                  <label>Rental Order</label>
                  <select class="form-control" name="rentalOrder">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                </div>
                           
                           <div class="form-group">
                  <label>Security Cheque</label>
                  <select class="form-control" name="securityCheque">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                </div>

                </div>
                  </div>
             
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right" >Create Customer</button>
              </div>
              <!-- /.box-footer -->
            </form>
       
           </div>
           <!-- /.box header -->
        </div>
        <!-- ./box -->
        
    </section>
    <!-- /.content -->
  </div>

  

    <?php } else {
   echo 'You are not authorized to access this page, please login. <br/>';
}
    
    include 'includes/footer.php';    
    ?>

    <!-- If registration successfull show everything ok info -->
                      <?php if(isset($_GET['success'])) {?>
                        <script>
                        showAlert("Success", "Customer created with ID " + <?php echo $_GET['success'];?>, "success");
                        </script>
                        <?php }?>

                    <!-- if registration error show this -->
                        <?php if(isset($_GET['error'])) {?>
                        <script>
                        showAlert("Error", "Oops! Some error ocurred.", "error");
                        </script>
                        <?php }?>   

<script src="plugins/jQuery/jQuery-validate.js"></script>
<script type="text/javascript">
		$.validator.setDefaults( {
			submitHandler: function (form) {
			//	alert( "submitted!" );
        
//$("form#newCustomer").submit(function(){
        var formData = new FormData( form );
    //action="add_recieving.php"
        console.log("going to submit");
        $.ajax({
           //url: window.location.pathname,
            url: 'example_customer_add.php',
            type: 'POST',
            data: formData,
          // async: false,
            beforeSend: function() {
            document.getElementById("loader").style.display = "block";
              
            },
            
            success: function (data) {
              //console.log("success");
                console.log(data);
                if(data === "Success") {  window.open('addRentalQuotation.php');}
                else alert(data);
               // location.reload(); 
            },
            error: function(data){
               console.log("error");
                console.log(data);
            },
            cache: false,
            contentType: false,
            processData: false
        }).complete(function() {
                document.getElementById("loader").style.display = "none";
            });

        return false;
//});
			}
		} );

		$( document ).ready( function () {
			$( "#newCustomer" ).validate( {
				rules: {
					firstName: "required",
					lastName: "required",
					company: "required",
					primPhone: "required",
          creditLimit: {
            required: true,
            maxlength: 10
          },
					email: {
						required: true,
						email: true
					},
          gst: {
            required: true,
           minlength: 15,
           maxlength: 15
          },
          cin: {
            required: true,
            maxlength: 15
          },
          billAddPin: {
            required: true,
            minlength: 6,
            digits: true,
           maxlength: 6
          },
          mailAddPin: {
            required: true,
            minlength: 6,
            digits: true,
           maxlength: 6
          }
					
				},
				messages: {
					firstName: "Please enter firstname",
					lastName: "Please enter lastname",
					email: "Please enter a valid email address",
          primPhone: "Atleast one contact is required",
          company: "Please enter company name",
          primPhone: "Please enter contact number",
          creditLimit: "Please enter a credit limit",
          gst: "Please enter a valid GST",
          cin: "Please enter a valid CIN"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
			} );

		
		} );
	</script>