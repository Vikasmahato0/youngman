
<?php include ("includes/header.php");?>
<?php
    $pending = $_GET['pending'];
    $email = $_GET['email'];


    $sql = '';

   /* if($type === 'dispatch')     $sql = "SELECT  table_challan.challan_id, table_challan.job_order, table_quotation.delivery_date 
                                        FROM table_challan,table_quotation 
                                        WHERE table_challan.job_order=table_quotation.job_order 
                                        AND table_challan.recieving ='0' AND  table_challan.pickup_loc_id = ? AND table_challan.type = 1";
    else if($type === 'pickup')  $sql = "SELECT  table_challan.challan_id, table_challan.job_order, table_quotation.delivery_date 
                                        FROM table_challan,table_quotation 
                                        WHERE table_challan.job_order=table_quotation.job_order 
                                        AND table_challan.recieving ='0' AND  table_challan.delivery_loc_id = ? AND (table_challan.type = 2 OR table_challan.type = 3)";


    if($qr  = $mysqli->prepare($sql )){
        $qr->bind_param('s', $warehouse);
         $qr->execute();
         $qr->store_result();
         $qr->bind_result($challan_id, $job_order, $delivery_date);  
        
    }else echo $mysqli->error;*/
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $pending." ".$email; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Planning</a></li>
        <li><a href="#">Stats</a></li>
        <li class="active">Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Pending</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            
          </div>
        </div>
           <?php if ($pending === 'quot'): ?>
                <div class="box-body">
                    <table id="godown" class="table table-bordered table-hover">
                        <thead>
                    <tr>
                        <th>ID</th>
                        <th>Type</th>
                        <th>Customer Name</th>
                    
                        <th>View</th>
                        
                        </tr>
                        </thead>
                        <tbody>     
                            <?php
                        if($stmt = $mysqli->prepare("SELECT q.s_no, q.type, q.customer_name,q.total, c.credit_limit, c.outstanding FROM table_quotation AS q, qb_cache_customer AS c WHERE status='quot' AND createdby = ? AND c.customer_id = q.customer_id ORDER BY timestamp DESC")){
                        $stmt->bind_param('s',  $email); // Bind "$email" to parameter.
                        $stmt->execute(); // Execute the prepared query.
                        $stmt->store_result();
                        $stmt->bind_result($s_no, $type, $customer_name, $total, $credit_limit, $outstanding); // get variables from result.
                        // $stmt->fetch();                            
                                                    
                        while($stmt->fetch())
                        {
                        ?>
                            
                        <tr>
                            <td class="sNo"><?php echo $s_no; ?></td>
                            <td><?php echo $type; ?></td>
                            <td><?php echo $customer_name; ?></td>
                        
                    
                            <td><a class="btn btn-block btn-default" href="<?php 
                                if($type=="Rental"){echo "viewquotation.php?id=".$s_no;}else{echo "view_sales_quotation.php?id=".$s_no;}
                                ?>"><i class="fa fa-eye"></i> View</a></td>
                        
                        </tr>
                        <?php 
                        }  $stmt->close();                      }else {echo "ERROR";}
                        ?>
                    
                            
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                        <th>Status</th>
                        <th>Customer Name</th>
                        
                            <th>View</th>
                        
                    </tr>
                    </tfoot>
                 </table>
                </div>
                </div>
            <?php endif; ?>

             <?php if ($pending === 'order'): ?>
                <div class="box-body">
              <table id="orders" class="table table-bordered table-hover">
                <thead>
                <tr>
                
                  <th>Customer Name</th>
                  <th>Job Order</th>
                  <th>View Order</th>
                </tr>
                </thead>
                <tbody>
                <?php
                 if($stmt = $mysqli->prepare("SELECT customer_name, job_order, s_no  FROM table_quotation WHERE status = 'order' AND   createdby = ? ORDER BY timestamp DESC")){
                   $stmt->bind_param('s',  $email); // Bind "$email" to parameter.
                   $stmt->execute(); // Execute the prepared query.
                   $stmt->store_result();
                   $stmt->bind_result($customer_name, $job_order, $s_no); // get variables from result.
                  // $stmt->fetch();                            
                                              
                while($stmt->fetch())
                {
                ?>
                <tr> 
                    
                  <td><?php echo $customer_name; ?></td>
                     <td><?php echo $job_order; ?></td>
                <td><a class="btn btn-block btn-default" href="vieworder.php?id=<?php echo $s_no; ?>"><i class="fa fa-eye"></i> View</a></td>
                </tr>
                <?php 
                }
                       $stmt->close();                      }else {echo "ERROR";}
                ?>
                </tbody>
                <tfoot>
                <tr>
                
                  <th>Description</th>
                  <th>Job Order</th>
                  <th>View Order</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <?php endif; ?>

            <?php if ($pending === 'movement'): ?>

<?php  $movement  = $mysqli->prepare("SELECT c.job_order, c.pickup_loc_id  from table_challan AS c, table_quotation AS q WHERE c.job_order = q.job_order AND c.type=1 AND c.recieving='0' AND q.createdby = ?" );
                   $movement->bind_param('s', $email);
                   $movement->execute();
                   $movement->store_result();
                   $movement->bind_result($job, $warehouse);  
                  
                   
                   ?>

  <div class="box-body">
              <table id="orders" class="table table-bordered table-hover">
                <thead>
                <tr>
                
                  <th>Job Order</th>
                  <th>Warehouse</th>
                 
                </tr>
                </thead>
                <tbody>
                    <?php while( $movement->fetch()) {?>
                        <tr>
                            <td><?php echo $job; ?></td>
                            <td><?php echo $warehouse; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                
                  <th>Job Order</th>
                  <th>Warehouse</th>
                 
                </tr>
                </tfoot>
              </table>
            </div>
            <?php endif; ?>

      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
