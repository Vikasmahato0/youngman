
<?php include ("includes/header.php");?>

<?php 

$qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE type = 1 AND recieving='0' AND pickup_loc_id = 'Delhi'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result($count_pending_dispatch_delhi);  
         $qr->fetch();
         $qr->close();

 $qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE (type = 2 OR type = 3)AND recieving='0' AND delivery_loc_id = 'Delhi'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result( $count_pending_pickup_delhi);  
         $qr->fetch();
         $qr->close(); 

 $qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE type = 1 AND recieving='0' AND pickup_loc_id = 'Mumbai'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result($count_pending_dispatch_Mumbai);  
         $qr->fetch();
         $qr->close();

 $qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE (type = 2 OR type = 3)AND recieving='0' AND delivery_loc_id = 'Mumbai'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result( $count_pending_pickup_Mumbai);  
         $qr->fetch();
         $qr->close(); 

 $qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE type = 1 AND recieving='0' AND pickup_loc_id = 'Chennai'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result($count_pending_dispatch_Chennai);  
         $qr->fetch();
         $qr->close();

 $qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE (type = 2 OR type = 3)AND recieving='0' AND delivery_loc_id = 'Chennai'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result( $count_pending_pickup_Chennai);  
         $qr->fetch();
         $qr->close(); 
$qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE type = 1 AND recieving='0' AND pickup_loc_id = 'Kolkata'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result($count_pending_dispatch_Kolkata);  
         $qr->fetch();
         $qr->close();

 $qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE (type = 2 OR type = 3)AND recieving='0' AND delivery_loc_id = 'Kolkata'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result( $count_pending_pickup_Kolkata);  
         $qr->fetch();
         $qr->close(); 

$qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE type = 1 AND recieving='0' AND pickup_loc_id = 'Ahmedabad'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result($count_pending_dispatch_Ahmedabad);  
         $qr->fetch();
         $qr->close();

 $qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE (type = 2 OR type = 3)AND recieving='0' AND delivery_loc_id = 'Ahmedabad'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result( $count_pending_pickup_Ahmedabad);  
         $qr->fetch();
         $qr->close(); 
                  $qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE type = 1 AND recieving='0' AND pickup_loc_id = 'Bangalore'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result($count_pending_dispatch_Bangalore);  
         $qr->fetch();
         $qr->close();

 $qr  = $mysqli->prepare("SELECT COUNT(challan_id) FROM table_challan WHERE (type = 2 OR type = 3)AND recieving='0' AND delivery_loc_id = 'Bangalore'" );
         $qr->execute();
         $qr->store_result();
         $qr->bind_result( $count_pending_pickup_Bangalore);  
         $qr->fetch();
         $qr->close(); 
                        
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Statistics
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Planning</a></li>
        <li class="active">Stats</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
           
          </div>
        </div>
        <div class="box-body">
            <table id="orders" class="table table-bordered table-hover">
                <thead>
                <tr>
                
                  <th>Location</th>
                  <th>Pending Dispatch</th>
                  <th>Pending Pickups</th>
                </tr>
                </thead>
                <tbody>
               <tr>
                <td>Delhi</td>
                <td name = "delhi_dispatch"><a href = "detailed_ops.php?warehouse=Delhi&type=dispatch" ><?php echo $count_pending_dispatch_delhi; ?></a></td>
                <td name = "delhi_pickup"><a href = "detailed_ops.php?warehouse=Delhi&type=pickup" ><?php echo $count_pending_pickup_delhi; ?></a></td>
               </tr>
                <tr>
                <td>Mumbai</td>
                 <td name = "mumbai_dispatch"><a href = "detailed_ops.php?warehouse=Mumbai&type=dispatch" ><?php echo $count_pending_dispatch_Mumbai; ?></a></td>
                <td name = "mumbai_pickup"><a href = "detailed_ops.php?warehouse=Mumbai&type=pickup" ><?php echo $count_pending_pickup_Mumbai; ?></a></td>
               </tr>
                <tr>
                <td>Chennai</td>
                 <td name = "chennai_dispatch"><a href = "detailed_ops.php?warehouse=Chennai&type=dispatch" ><?php echo $count_pending_dispatch_Chennai; ?></a></td>
                <td name = "chennai_pickup"><a href = "detailed_ops.php?warehouse=Chennai&type=pickup" ><?php echo $count_pending_pickup_Chennai; ?></a></td>
               </tr>
                <tr>
                <td>Ahmedabad</td>
                 <td name = "ahmedabad_dispatch"><a href = "detailed_ops.php?warehouse=Ahmedabad&type=dispatch" ><?php echo $count_pending_dispatch_Ahmedabad; ?></a></td>
                <td name = "ahmedabad_pickup"><a href = "detailed_ops.php?warehouse=Ahmedabad&type=pickup" ><?php echo $count_pending_pickup_Ahmedabad; ?></a></td>
               </tr>
                <tr>
                <td>Bangalore</td>
                 <td name = "bangalore_dispatch"><a href = "detailed_ops.php?warehouse=Bangalore&type=dispatch" ><?php echo $count_pending_dispatch_Bangalore; ?></a></td>
                <td name = "bangalore_pickup"><a href = "detailed_ops.php?warehouse=Bangalore&type=pickup" ><?php echo $count_pending_pickup_Bangalore; ?></a></td>
               </tr>
                <tr>
                <td>Kolkata</td>
                 <td name = "kolkata_dispatch"><a href = "detailed_ops.php?warehouse=Kolkata&type=dispatch" ><?php echo $count_pending_dispatch_Kolkata; ?></a></td>
                <td name = "kolkata_pickup"><a href = "detailed_ops.php?warehouse=Kolkata&type=pickup" ><?php echo $count_pending_pickup_Kolkata; ?></a></td>
               </tr>
                </tbody>
               
              </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
      
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
