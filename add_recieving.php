<?php

//include("includes/dbcon.php");
include("secure/db_connect.php");
include("secure/config.php");
include dirname(__FILE__) . '/qb_functions.php';

ini_set('display_errors',1);
error_reporting(E_ALL);

mysqli_autocommit($mysqli, false);

$challan_id = $_POST['bookId'];
$transporter = $_POST['transporter'];
$gr_no = $_POST['gr_no'];
$amt = $_POST['amt'];
$recieving = '';
$job_order = $_POST['jobOrder'];
$current_date = date("Y-m-d"); 
$current_time = date("H:i:s");

$recieving_date = $_POST['recieving_date'];
$pre =  false;
$flag =true;
if(DEBUG) echo "Start falg = ";
if($flag == true) if(DEBUG) echo "true<br>";
       else if(DEBUG) echo "false<br>";
$remarks = null;
$failed = 0; //1 for journal, 2 for estimate, 3 for first invoice pre

//if(DEBUG) echo "Amt: ".$amt;

   //Check if it is pre       
       $q = "SELECT customer_local.category FROM customer_local,table_quotation WHERE customer_local.customer_id = table_quotation.customer_id AND table_quotation.job_order = ? ";
       if($st = $mysqli->prepare($q)){
        $st->bind_param('s', $job_order);
        $st ->execute();
        $st ->store_result();
        $st ->bind_result($category);   
        $st->fetch();
        }else if(DEBUG) echo $mysqli->error;       
        if($category=="Pre"){    
                $pre =  true;
        }
       


if(isset($_POST["recieving_submit"]) ){
    $code = $_POST['code'];
    $desc = $_POST['desc'];
    $ok_qty = $_POST['ok_qty'];
    $missing_qty =$_POST['missing_qty'];
    $damage_qty = $_POST['damage_qty'];
    $warehouse_id = $_POST['warehouse_id'];
    $remarks = $_POST['remarks'];
    
    for($i=0; $i<count($code); $i++){
        
      $fake_ch = "INSERT INTO  challan_item_relation (challan_id, item_id, item_description, quantity, missing, damage, job_order) VALUES(?, ?, ?, ?, ?, ?, ? ) ON DUPLICATE KEY UPDATE quantity = quantity + VALUES(quantity), missing = missing + VALUES(missing) , damage = damage + VALUES(damage)";

          if($fc = $mysqli->prepare($fake_ch)){
            $fc->bind_param('sssssss' , $challan_id, $code[$i], $desc[$i], $ok_qty[$i], $missing_qty[$i], $damage_qty[$i], $job_order );
            if(! $fc->execute()) { if(DEBUG) echo "insert challan_item_rel failed ".$fc->error; $flag =false; if(DEBUG) echo "62 falg = ";
    if($flag == true) if(DEBUG) echo "true<br>";
        else if(DEBUG) echo "false<br>"; }
            }else { if(DEBUG) echo $mysqli->error; $flag = false;  }

   $fake_q = "UPDATE location_item_relation SET quantity = quantity - ( ? + ? + ?), missing = missing + ? WHERE location_id = ? AND item_id = ? ";
       
        if($fc = $mysqli->prepare($fake_q)){
            $fc->bind_param('ssssss',$ok_qty[$i], $damage_qty[$i],$missing_qty[$i], $missing_qty[$i], $job_order, $code[$i] );
            if ( !$fc->execute()) $flag = false;
          }else { if(DEBUG) echo $mysqli->error; $flag = false; }
     
       $fake_s = "UPDATE location_item_relation SET quantity = quantity + ?, damaged =damaged + ? WHERE location_id = ? AND item_id = ? ";

        if($fc = $mysqli->prepare($fake_s)){
            $fc->bind_param('ssss',$ok_qty[$i], $damage_qty[$i], $warehouse_id, $code[$i] );
           if ( !$fc->execute()) $flag = false;
          }else { if(DEBUG) echo $mysqli->error; $flag = false; }        
        
  

    }
    
    if(!addEstimate($challan_id, $job_order, $code, $desc, $missing_qty, $damage_qty )){
            //HANDLE FAILURE OF ESTIMATE CREATION
        $flag = false;
        if(DEBUG) echo "<br>85 falg = ";
    if($flag == true) if(DEBUG) echo "true<br>";
       else if(DEBUG) echo "false<br>";
        $failed = 2;
        }

}

$recieving_date = strtotime($recieving_date);

$recieving_date = date("Y-m-d", $recieving_date); //SQL format date

if(DEBUG) echo "Recieving Date: ".$recieving_date;

$target_dir = "uploads/";
  $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $extension = end(explode(".", $_FILES["fileToUpload"]["name"]));
    $path=md5($current_date.$current_time).'.'.$extension;

$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])||isset($_POST["recieving_submit"])  ) {
 $uploadOk = 1;
}
// Check if file already exists
if (file_exists($target_file)) {
    if(DEBUG) echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000000) {
    if(DEBUG) echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "pdf"   ) {
    if(DEBUG) echo "Sorry, only JPG, JPEG, PNG, GIF & PDF files are allowed. Your file is a ".$imageFileType;
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    if(DEBUG) echo "Sorry, your file was not uploaded.";
    $flag=false;
    if(DEBUG) echo "<br>130 falg = ";
if($flag == true) if(DEBUG) echo "true<br>";
       else if(DEBUG) echo "false<br>";
// if everything is ok, try to upload file
} 
else {
    
    
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "uploads/".$path) && $flag)
    {
        
        $recieving = "uploads/". $path;
              
        
        $query = "UPDATE table_challan 
                    SET recieving = ?,
                    recieving_date = ?,
                    transporter = ?, 
                    gr_no = ?, 
                    amt = ?,
                    status = 1,
                    remarks = ?
                    WHERE challan_id = ? ";
        
        if($jo  = $mysqli->prepare( $query )){
        $jo ->bind_param('sssssss',$recieving, $recieving_date,$transporter, $gr_no, $amt, $remarks, $challan_id);
        
        }else if(DEBUG) echo $mysqli->error;
        
        
if($jo ->execute())
{
    if(isset($_POST["recieving_submit"])) if(DEBUG) echo "<br> billing entry is not required";
    
    if( ! isset($_POST["recieving_submit"]) ){
    $select_items = "SELECT item_code FROM invoice_item_feed WHERE job_order = ? AND challan_id= ? ";
    
      if($stmt  = $mysqli->prepare( $select_items )){
        $stmt ->bind_param('ss',$job_order, $challan_id);
        $stmt ->execute();
        $stmt ->store_result();
        $stmt ->bind_result($item_code);   
        }else {if(DEBUG) echo $mysqli->error; $flag = false;}
    
        $last_start_date = null;
        $last_end_date = $recieving_date;
        $month_end = new DateTime('last day of this month');
        $month_end = $month_end->format('Y-m-d');
        $first_day_of_next_month = date("Y-m-d", strtotime("+1 day", strtotime($month_end)));
        
        if($pre){
            $last_start_date = $recieving_date;
            $last_end_date = $first_day_of_next_month;
        }
    
    
    
    while($stmt ->fetch()){
        $ic = $item_code;
  
         $select_from_invoice = "SELECT invoice_item_feed.qty, table_quotation_item.unit_price, table_quotation.pickup_date, table_quotation.duration_billing, customer_local.category FROM invoice_item_feed, table_quotation, table_quotation_item, customer_local WHERE invoice_item_feed.job_order = ? AND invoice_item_feed.challan_id = ? AND invoice_item_feed.item_code = ? AND invoice_item_feed.job_order = table_quotation.job_order AND table_quotation.s_no = table_quotation_item.s_no AND table_quotation_item.item_code = ? AND table_quotation.customer_id = customer_local.customer_id";

         if($s = $mysqli->prepare($select_from_invoice)){
            $s ->bind_param('ssss',$job_order, $challan_id, $item_code, $ic);
            $s ->execute();
            $s ->store_result();
            $s ->bind_result($qty, $unit_price, $pickup_date, $duration_billing, $category );   


         }else { if(DEBUG) echo $mysqli->error; $flag = false; }
        while($s->fetch()){


            $query1 = "INSERT INTO table_billing (job_order, challan_no, category, pickup_date, period, item_code, unit_price, start_date, qty, last_end_date, last_start_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";
            
     if($sm = $mysqli->prepare($query1)){
            $sm ->bind_param('sssssssssss',$job_order, $challan_id, $category, $pickup_date, $duration_billing, $ic, $unit_price, $recieving_date, $qty, $last_end_date, $last_start_date );
         
         if(! $sm->execute() ) { if(DEBUG) echo "billing failed ".$sm->error; $flag=false; }
             }else{ if(DEBUG) echo "billing prepare failed".$mysqli->error; $flag = false; }
            
        }
    }  
    }
   if(create_journal_entry($amt, $challan_id, $gr_no, $transporter, $job_order)){
        mysqli_commit($mysqli);
    
       if($pre && (! isset($_POST["recieving_submit"] )) ) {if(DEBUG) echo "going to make first invoice"; if ( ! first_invoice_pre($job_order, $challan_id, $recieving_date) ) {if(DEBUG) echo "Invoice failed"; $flag=false; $failed=3;//mysqli_rollback($mysqli);
       } }else if(DEBUG) echo "<br> this is closure so no invoicing is needed";
       
   }else{
       if(DEBUG) echo "creating journal entry faild";
       $failed = 2;
       $flag = false;
     
   }

} else{
    if(DEBUG) echo "ERROR<br>".$jo->error;    
    if(DEBUG) echo $query;
    $flag = false;
}
     
        
        
    }
    else
    {
         if(DEBUG) echo "Sorry, there was an error uploading your file.".$_FILES['fileToUpload']['error'];
       if($flag == true) if(DEBUG) echo "true";
       else if(DEBUG) echo "false";
       $flag = false;
     
    }
}

if($flag==false) {
    mysqli_rollback($mysqli);
    if(DEBUG) echo "Your request could not be completed";
  else  header("Location: dashboard_movement.php?error=error uploading your file.");
} else {
    mysqli_commit($mysqli);
    if(DEBUG) echo "Your request completed successfully";
  else  header("Location: dashboard_movement.php?success=1");
}
?>
