<?php

require_once dirname(__FILE__) . '/config.php';

require_once dirname(__FILE__) . '/views/header.tpl.php';
include 'secure/db_connect.php';
?>

    <pre>

<?php

$mobilisation = '';
$rental = '';
$manpower = '';

$last_bill_end_date = new DateTime('last day of this month');
$last_date = $last_bill_end_date->format('j-M-Y');

$variable_for_sql_date_difference = $last_bill_end_date->format('Y-m-d');

$last_bill_end_date = $last_bill_end_date->format('Y-m-d');
$last_bill_end_date = date("Y-m-d", strtotime("+1 day", strtotime($last_bill_end_date)));

$ItemService = new QuickBooks_IPP_Service_Term();
$items = $ItemService->query($Context, $realm, "SELECT * FROM Item WHERE Name = 'Rental of  Product'");

foreach ($items as $Item)
{
	print('Id=' . $Item->getId() . ' Name: ' . $Item->getName() . '<br>');
    $rental = QuickBooks_IPP_IDS::usableIDType($Item->getId());
}

$items = $ItemService->query($Context, $realm, "SELECT * FROM Item WHERE Name = 'Manpower Costs'");

foreach ($items as $Item)
{
	print('Id=' . $Item->getId() . ' Name: ' . $Item->getName() . '<br>');
    $manpower = QuickBooks_IPP_IDS::usableIDType($Item->getId());
}

$items = $ItemService->query($Context, $realm, "SELECT * FROM Item WHERE Name = 'Mobilisation and Demobilisation Costs'");

foreach ($items as $Item)
{
	print('Id=' . $Item->getId() . ' Name: ' . $Item->getName() . '<br>');
    $mobilisation = QuickBooks_IPP_IDS::usableIDType($Item->getId());
}


$job_order = "SELECT DISTINCT(table_billing.job_order), table_quotation.qb_id, table_quotation.first_bill, table_quotation.freight From table_billing, table_quotation WHERE table_billing.period = 'Days AND table_billing.job_order = table_quotation.job_order";                
        if($jo  = $mysqli->prepare( $job_order )){
        $jo ->execute();
        $jo ->store_result();
        $jo ->bind_result($job_order, $qb_id, $first_bill, $freight);      
        }else echo $mysqli->error;
        while($jo->fetch()){
            
            $InvoiceService = new QuickBooks_IPP_Service_Invoice();

            $Invoice = new QuickBooks_IPP_Object_Invoice();

    
        echo "<br><b>".$job_order."</b><br>";
    
        $query = "SELECT DATEDIFF( ? ,b.last_end_date) AS DiffDate,b.category, b.period, b.item_code, SUM(b.qty), b.unit_price, b.last_start_date, b.last_end_date, b.active, table_item.name FROM table_billing AS b, table_item WHERE job_order = ? AND Month(last_end_date) <= Month(CURRENT_TIMESTAMP) AND table_item.item_code = b.item_code GROUP BY b.item_code, b.last_end_date";
    
            if($item  = $mysqli->prepare( $query )){
                $item->bind_param('ss', $variable_for_sql_date_difference, $job_order);
                $item ->execute();
                $item ->store_result();
                $item ->bind_result($date_difference , $category, $period, $item_code, $qty, $unit_price, $last_start_date, $last_end_date, $active, $description);      
                }else echo $mysqli->error;

                while($item->fetch()){
                    
                  
                    $ref = $rental;
                        
                    if ($item_code=="MANPOWER"){
                       
                        $ref = $manpower;
                    }

                    echo $item_code. "Description: ".$description."<br>";
                    
                    $last_bill_start_date = $last_end_date;
                   
                    
                    
                    $last_end_date = date('j-M-Y', strtotime($last_end_date));
                      $description = $description." | From:".$last_end_date." | To:".$last_date;
                   
                   
                    
                    $unit_price = ($unit_price / date("t") ) * ($date_difference + 1);
                    
                    $unit_price = number_format((float)$unit_price, 2, '.', '');
                   
                    echo "UNIT PRICE : ".$unit_price."<br>";
                    
                    $Line = new QuickBooks_IPP_Object_Line();
                    $Line->setDetailType('SalesItemLineDetail');
                    $Line->setAmount($qty*$unit_price);
                    $Line->setDescription($description);
                    
                    $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
                    $SalesItemLineDetail->setItemRef($ref);
                    $SalesItemLineDetail->setUnitPrice($unit_price);
                    $SalesItemLineDetail->setQty($qty);
                    
                    $Line->addSalesItemLineDetail($SalesItemLineDetail);
                    
                    $Invoice->addLine($Line);
                    
                    
                    if($swap = $mysqli->prepare("UPDATE table_billing SET last_end_date = ? , last_start_date = ? WHERE job_order = ? AND item_code = ? AND last_end_date = ?")){
                             $swap->bind_param('sssss',$last_bill_end_date  ,$last_bill_start_date  ,$job_order, $item_code, $last_bill_start_date);
                            $swap->execute();
                        }else echo $mysqli->error;
                    

                }
            
            if(!$first_bill){
                echo "Freight".$freight."<br>";
                
                 $Line = new QuickBooks_IPP_Object_Line();
                    $Line->setDetailType('SalesItemLineDetail');
                    $Line->setAmount($freight * 1);
                    $Line->setDescription("One time mobilisation and demobilisation charges");
                    
                    $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
                    $SalesItemLineDetail->setItemRef($mobilisation);
                    $SalesItemLineDetail->setUnitPrice($freight);
                    $SalesItemLineDetail->setQty(1);
                    
                    $Line->addSalesItemLineDetail($SalesItemLineDetail);
                    
                    $Invoice->addLine($Line);
            }
            
            $Invoice->setCustomerRef($qb_id);
                    
                    if ($resp = $InvoiceService->add($Context, $realm, $Invoice))
                    {
                        print('Our new Invoice ID is: [' . $resp . ']');
                        
                        echo "UPDATE table_quotation SET first_bill = 1 WHERE job_order = ".$job_order."<br>";
                        
                        if($made_bill = $mysqli->prepare("UPDATE table_quotation SET first_bill = 1 WHERE job_order = ?")){
                             $made_bill->bind_param('s', $job_order);
                            $made_bill->execute();
                        }else echo $mysqli->error;
                        
                        
                    }
                    else
                    {
                        print($InvoiceService->lastError());
                    }
    
}

?>

</pre>

    <?php

require_once dirname(__FILE__) . '/views/footer.tpl.php';