<?php include ("includes/header.php");?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
<link href="dist/css/jquery-ui.min.css" rel="stylesheet">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Items at Warehouses
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Items</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        <div class="input-group input-group-sm col-sm-4">
            <div class="form-group">
            <label for="item">Items Search</label>
            <input type="text" class="form-control" name="item" id="item" placeholder="Enter Item Name" autocomplete="off">
            </div>
             <div id="suggesstion-box"></div>
         </div>

        <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Search Results</h3>
            </div>
            <!-- /.box-header -->
              <div id="location_details" class="box-body table-responsive no-padding">
            
         
                  
              </div>
              
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
        
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>

<!--<script src="dist/js/item_at_warehouse.js"></script> -->
<script src="dist/js/jquery-ui.min.js"></script>
<script>
    $(document).ready(function() {
        $("#item").keyup(function() {
            var data = $(this).val();
            data = encodeURI(data).replace(/&/g, "%26");
            $.ajax({
                type: "POST",
                url: "ajax/get_item_names.php",
                data: 'keyword=%' + data + '%',
                beforeSend: function() {
                    $("#item").css("background", "#FFF url(img/ LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data) {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#name").css("background", "#FFF");
                    console.log(data);
                }
            });
        });
    });

    function selectItem(job) {
        item = encodeURI(job).replace(/&/g, "%26");
        console.log(item);
        $("#suggesstion-box").hide();
        $("#item").val(item);
        $.ajax({
            "type": "POST",
            "url": "ajax/get_item_at_warehouse.php",
            data: 'item_id=' + item,
            "success": function(data) {
                $("#location_details")
                    .html(data);
            }
        });
    }
</script>