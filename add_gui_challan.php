<?php
include("secure/functions.php");
include("secure/config.php");
sec_session_start();

if(isset($_POST['submit']) && checkCsrf($_POST['csrf']) && $_SESSION['role']==='planning')
{

if(DEBUG) {error_reporting(E_ALL); ini_set('display_errors', 1);}
 $dbConnection = mysqli_connect("localhost","root","9.56418x10^19","youngman_qb");
    // Check connection
    if (mysqli_connect_errno())
      {
      if (DEBUG) echo "Failed to connect to MySQL: " . mysqli_connect_error();
      }
    if (DEBUG) echo "<br>connected";
    
    mysqli_autocommit($dbConnection, false);
    
    if (DEBUG) echo "<br>autocommit disabled<br>";

    $location_from      = $_POST['from'];
    $location_to        = $_POST['to'];
    $job_order          = $_POST['job_order'];
    $typeofchallan      = $_POST['typeofchallan'];
    $type               = "Item";
    $item_code          = $_POST['itemNo'];
    $item_description   = $_POST['itemName'];
    $item_quantity      = $_POST['quantity'];
    $app_price          = $_POST['price'];
    $total_price        = $_POST['total'];
    $challan_tot        = $_POST['subTotal'];
    
    $item_code_BOM      = $_POST['itemCodeBOM'];
    $item_qty_BOM       = $_POST['itemQtyBOM'];
    
    if (DEBUG) echo  $location_from." ".$location_to." ". $job_order;
    
     $flag = true;
    
     $query1 = "INSERT INTO `table_challan`( `pickup_loc_id`, `delivery_loc_id`,`type`, `job_order`, `challan_tot`) VALUES ('$location_from','$location_to', '$typeofchallan', '$job_order', $challan_tot)";
     
     $challan_id="";

     if (mysqli_query($dbConnection, $query1)) {
        $challan_id = mysqli_insert_id($dbConnection);
        if (DEBUG) echo "<br>New Challan ID is: " .  $challan_id;
     } else {
         $flag = false;
         if (DEBUG) echo "<br>Error details: " . mysqli_error($dbConnection)."<br>".$query1;
     }
    
    for($i=0;$i<count($item_code);$i++){
       
        
        $update_to ="INSERT INTO location_item_relation (location_id, item_id, quantity) VALUES  ('$location_to','$item_code[$i]', $item_quantity[$i]) ON DUPLICATE KEY UPDATE quantity =( quantity + $item_quantity[$i])";
        
        if (DEBUG) echo "UPDATE TO".$update_to."<br>";
        
         $update_from ="INSERT INTO location_item_relation (location_id, item_id, quantity) VALUES  ('$location_from','$item_code[$i]', $item_quantity[$i] )
        ON DUPLICATE KEY UPDATE
        quantity = (quantity-$item_quantity[$i])";
        
        if (DEBUG) echo "UPDATE FROM".$update_from."<br>";

        $query1_relation = "INSERT INTO `challan_item_relation`(`challan_id`, `item_id`,`item_type`, `item_description`, `job_order`, `quantity`, `unit_price`, `total_price`, `challan_type`) VALUES ('$challan_id','$item_code[$i]','$type' , '$item_description[$i]','$job_order',  $item_quantity[$i],
          $app_price[$i], $total_price[$i], '$typeofchallan')";
        
      //  $result1 = mysqli_query($dbConnection, $update_to);
        
         if (mysqli_query($dbConnection, $update_to)) {
             if (DEBUG) echo "<br> Destinatin updated";
         } else {
            $flag = false;
            if (DEBUG) echo "<br>Error details: " . mysqli_error($dbConnection)."<br>".$update_to;
        }
        
         if (mysqli_query($dbConnection, $update_from)) {
             if (DEBUG) echo "<br> Source updated";
         } else {
            $flag = false;
            if (DEBUG) echo "<br>Error details: " . mysqli_error($dbConnection)."<br>".$update_from;
        }
        
         if (mysqli_query($dbConnection, $query1_relation)) {
             if (DEBUG) echo "<br> Relation updated";
         } else {
            $flag = false;
            if (DEBUG) echo "<br>Error details: " . mysqli_error($dbConnection)."<br>".$query1_relation;
        }
        
        
        $non_negative = "SELECT quantity FROM location_item_relation WHERE location_id = '$location_from' AND item_id = '$item_code[$i]'";
        if (DEBUG) echo $non_negative;
        $row = mysqli_fetch_assoc( mysqli_query($dbConnection, $non_negative));
        
        if($row['quantity']<0) $flag=false;
        
    }
    
    for($i=0;$i<count($item_code_BOM);$i++){
        $invoice_item_feed = "INSERT INTO invoice_item_feed (`challan_id`, `job_order`, `item_code`, `qty`) VALUES ('$challan_id', '$job_order', '$item_code_BOM[$i]', '$item_qty_BOM[$i]' )";
        if (mysqli_query($dbConnection, $invoice_item_feed)) {
             if (DEBUG) echo "<br> Invoice Item feed updated";
         } else {
            $flag = false;
            if (DEBUG) echo "<br>Error details: " . mysqli_error($dbConnection)."<br>".$invoice_item_feed;
        }
        
        $challan_order_item_relation = "INSERT INTO challan_order_item_relation (`challan_id`, `item_code`, `qty`) VALUES ('$challan_id', '$item_code_BOM[$i]', '$item_qty_BOM[$i]' )";
        if (mysqli_query($dbConnection, $challan_order_item_relation)) {
             if (DEBUG) echo "<br> challan_order_item_relation updated";
         } else {
            $flag = false;
            if (DEBUG) echo "<br>Error details: " . mysqli_error($dbConnection)."<br>".$challan_order_item_relation;
        }
        
        $update_order_feed = "UPDATE order_item_feed SET qty = qty-$item_qty_BOM[$i] WHERE item_code = '$item_code_BOM[$i]' AND job_order='$job_order'";
        
        if (mysqli_query($dbConnection, $update_order_feed)) {
             if (DEBUG) echo "<br>Order feed updated";
         } else {
            $flag = false;
            if (DEBUG) echo "<br>Error details: " . mysqli_error($dbConnection)."<br>".$update_order_feed;
        }
        
    }
    
    if ($flag) {
         mysqli_query($dbConnection, "DELETE FROM order_item_feed WHERE qty=0");
        mysqli_commit($dbConnection);
       
        if (DEBUG) echo "All queries were executed successfully";
     header("Location: dashboard_planning.php");
    } else {
        mysqli_rollback($dbConnection);
        if (DEBUG) echo "All queries were rolled back";
    } 
    
    
    
    
}


    ?>
