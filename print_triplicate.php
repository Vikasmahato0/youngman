<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Youngman | Challan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
    
<?php
 include("includes/dbcon.php");
include("secure/db_connect.php");
include("generate_challan_body.php");
$challan_total = 0;
$our_gst = '';

$job_order = $_GET['job'];
$challan_id = $_GET['id'];
$challan_type = '';

$pickup_location_id = '';
$delivery_location_id = '';

$pickup_address = '';
$delivery_address = '';
$challan_amt = '';
$timestamp = '';
$remarks = '';
    
$query_loc_id = "SELECT * from table_challan WHERE challan_id = '$challan_id'";

$loc_ids = mysqli_query($con, $query_loc_id);
                    while($locations=mysqli_fetch_array($loc_ids)){
                        
                        $pickup_location_id = $locations['pickup_loc_id'];
                        $delivery_location_id = $locations['delivery_loc_id']  ;   //JOB Order
                            $challan_type =  $locations['type'];
                        $challan_amt =  $locations['challan_tot'];
                         $timestamp = $locations['timestamp'];
                          $remarks = $locations['remarks'];
                    }


    $query_pickup_loc = "SELECT * from table_location WHERE location_id='$pickup_location_id''";

$pickup_location = mysqli_query($con, $query_pickup_loc);
    while($pick=mysqli_fetch_array($pickup_location)){
                        $pickup_address = $pick['address'].'<br>'.$pick['state']."<br>".$pick['pincode'];
                    }



if($challan_type == 1) {       
  $q = "SELECT qb_cache_customer.customer_name, table_quotation.delivery_address, table_quotation.delivery_pincode, table_quotation.contact_name, table_quotation.place_of_supply, state_codes.state, qb_cache_customer.billing_address, qb_cache_customer.billing_pincode, customer_local.gst_no FROM table_quotation , qb_cache_customer, state_codes, customer_local WHERE table_quotation.job_order = ? AND table_quotation.customer_id = qb_cache_customer.customer_id AND table_quotation.customer_id = customer_local.customer_id AND state_codes.state_num = table_quotation.place_of_supply";
    if ( $stmt = $mysqli->prepare( $q ) ) {
        $stmt->bind_param('s', $delivery_location_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result( $customer_name, $delivery_address, $delivery_pincode, $contact_name,  $place_of_supply, $state_of_supply, $billing_address, $billing_pincode, $gst);
        $stmt->fetch();
    }else echo $mysqli->error;
    
  
    
    
     $q = "SELECT  address, state, pincode FROM table_location WHERE location_id = ? ";
    if ( $stmt = $mysqli->prepare( $q ) ) {
        $stmt->bind_param('s', $pickup_location_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($address, $state, $pincode);
        $stmt->fetch();
    }else echo $mysqli->error;
    
    $pickup_address = $address."<br>". $state."<br>". $pincode;
    
    
    
}


$tot_items = $mysqli->prepare( "SELECT SUM(quantity) FROM `challan_item_relation` WHERE challan_id = ?" );
$tot_items->bind_param('s', $challan_id);
$tot_items->execute();
$tot_items->store_result();
$tot_items->bind_result($item_count);
$tot_items->fetch();


$challan_order_item_relation = $mysqli->prepare("SELECT c.item_code, c.qty, i.name FROM challan_order_item_relation AS c, table_item AS i WHERE challan_id = ? AND i.item_code = c.item_code");
$challan_order_item_relation->bind_param('s', $challan_id);
$challan_order_item_relation->execute();
$challan_order_item_relation->store_result();
$challan_order_item_relation->bind_result($item_code, $qty, $item_name);

  $pickup_state_code = '';
   switch($pickup_location_id) {
    case "Gnoida":
      $pickup_state_code = '09';
      $our_gst = '09AAACY4840M1ZV';
      break;
    case "Mumbai":
      $pickup_state_code = '27';
      $our_gst = '27AAACY4840M1ZX';
      break;
    case "Chennai":
      $pickup_state_code = '33';
      $our_gst = '33AAACY4840M1Z4';
      break;
    case "Kolkata":
      $pickup_state_code = '19';
      $our_gst = 'Not provided';
      break;
    case "Ahmedabad":
      $pickup_state_code = '24';
      $our_gst = '24AAACY4840M1Z3';
      break;
    case "Bangalore":
      $pickup_state_code = '29';
      $our_gst = '29AAACY4840M1ZT';
      break;
    default :
      $pickup_state_code = 'N/A';
      $our_gst = 'N/A';
      break;
  }
$interstate = false;
if($place_of_supply === $pickup_state_code ) $interstate = true;


$header = getHeader($interstate);
$body = getBody($interstate, $challan_id);
$footer = getFooter($interstate, $item_count, $challan_amt);
?>

<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;    
}
</style>

    
<body  onload="window.print();">
<div class="wrapper">
      <!-- Main content -->
    <!-- Main content -->
     <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
             <img src="logo.png">
             <medium class="pull-right">
              <?php
          
          if($challan_type=='0'){
              echo "Sales Challan";
          }elseif($challan_type=='1'){
              echo "Delivery Challan";
          }elseif($challan_type=='2'){
              echo "Pickup Challan";
          }
          
          ?>
               <small><strong><?php echo "<br>Dated: ".date('M j Y', strtotime($timestamp)); ?></strong></small>
              </medium>
          </h2>
        </div>
        <!-- /.col -->
      </div>

       <?php if($challan_type=='1') echo "<h5 style='text-align: center;'>Goods supplied under lease arrangement on returnable basis after completion of job (Not meant for sale)</h5>";  ?>
      <!-- info row -->
        <hr>
        
    <div class="row">
        <div class="col-xs-12 table-responsive">
        <table style="width:100%">
        <tr>
          <th>Youngman India Private Ltd</th>
          <th>Delivery Challan</th>
          <th>Original For Consignee</th>
        </tr>

        <tr>
          <td><?php echo $pickup_address;?><br><b>GSTIN: </b> <?php echo $our_gst; ?></td>
           <td><b>Challan No : </b><?php echo $challan_id; ?></td>
          <td><b>Date : </b><?php echo date('M j Y', strtotime($timestamp)); ?></td>
        </tr>

        <tr>
          <td><b>Consignee Name & Address</b></td>
           <td rowspan="6" colspan="2">Remarks:<br><?php echo $remarks; ?></td>
        </tr>
        <tr>
          <td><?php echo $delivery_address; ?></td>
        </tr>
      
        <tr>
          <td><b>Place of Supply: </b><?php echo  $state_of_supply; ?></td>
        </tr>
      </table>
</div>
</div>

     <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table">
          <thead>
          <?php echo $header; ?>
          </thead>
           
            <tbody>

                <?php echo $body; ?>
            
            </tbody>
             <?php echo $footer; ?>
          </table>
        </div>
        <!-- /.col -->
      </div>
         
<div class="row" style="width:100%">
 <h5 style='text-align: center;'><strong> Using these items the following can be set up</strong></h5>
</div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table">
            <thead>
            <tr>
        
                <th>Item Id</th>
                <th>Qty</th>
                
            </tr>
            </thead>
            <tbody>
                <?php while($challan_order_item_relation->fetch()) {
                   echo "<tr><td>$item_name</td><td>$qty</td></tr>";
                } ?>
              
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>

 <div class="row" style='text-align: right; padding: 15px;  right: 10px;'>
        <div>
  <p><b>For Youngman India Pvt. Ltd.</b></p>

  <br><br><br>

  <p>Authorized signatory</p>
  </div>
</div>

    </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
