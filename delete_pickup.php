<?php

include 'secure/db_connect.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $challan_id = $_POST['challan_id'];
     if($pick  = $mysqli->prepare( "DELETE FROM table_challan WHERE challan_id = ?" )){
        $pick->bind_param('s',$challan_id);
        $pick->execute();
         
        }else echo $mysqli->error;

}

$query = "SELECT job_order, challan_id, delivery_loc_id FROM table_challan WHERE type='2' AND recieving='0' ";

 if($pick  = $mysqli->prepare( $query )){
      //  $pick->bind_param('s',$job_order);
        $pick->execute();
        $pick->store_result();
        $pick->bind_result($job_order, $challan_id, $delivery_loc_id);   
        }else echo $mysqli->error;
?>
<?php include ("includes/header.php");?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Delete Pickups
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pickups</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
         <div class="row">

</div> 
        <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Pickups</h3>
        </div>
           
         
          <div class="box-body">
              <table id="godown" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Job Order</th>
                  <th>Warehouse</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
          <?php while($pick->fetch()){?>
                      <tr>
                <td><?php echo $job_order; ?></td>
                <td><?php echo $delivery_loc_id; ?></td>
                <td> <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" > <input type="hidden" name="challan_id" value="<?php echo $challan_id; ?>"> <button>Delete</button></form></td>
                
                </tr>
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
      
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
<script>
  
 $(document).ready(function() {
            $("#name").keyup(function() {
                $.ajax({
                    type: "POST",
                    url: "ajax/readCustomerFromCache.php",
                    data: 'keyword=' + $(this).val(),
                    beforeSend: function() {
                        $("#name").css("background", "#FFF url(img/ LoaderIcon.gif) no-repeat 165px");
                        //console.log($(this).val());
                    },
                    success: function(data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#name").css("background", "#FFF");
                        console.log("data");
                    }
                });
            });
        });

        function selectCustomer(id, name) {
            console.log(name);
            console.log(id);
            $("#id").val(id);
            $("#suggesstion-box").hide();
            $("#name").val(name);
          $.ajax({
                    type: "POST",
                    url: "ajax/get_job_for_customer.php",
                    data: {keyword: id,
                            pickup: 'true'},
                    beforeSend: function() {
                       // $("#name").css("background", "#FFF url(img/ LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function(response) {
                     
                     $("#job_for_cust").empty();
             
                    
                   // $("#job_for_cust").append(response);
                     $("#job_for_cust").append(response);
                
                    }
                });
        }


</script>
