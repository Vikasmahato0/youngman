<?php
function getHeader($interstate){
 return "
            <tr>
              <th>S. No.</th>
              <th>HSN</th>
              <th>Item Id</th>
              <th>Description</th>
              <th>Qty</th>
              <th>Unit</th>
              <th>Taxable Value</th>
              <th>CGST</th>
              <th>SGST</th>
              <th>IGST</th>
              <th>Central tax </th>
               <th>State tax </th>
                <th>Integrated tax </th>
              <th>Total</th>
            </tr>
           ";
}
function getFooter($interstate, $tot_qty, $tot_val){
  global $challan_total;
  return "
            <tr>
              <th>Total</th>
              <th></th>
              <th></th>
              <th></th>
              <th>$tot_qty</th>
              <th>Pcs</th>
              <th></th>
              <th></th>
               <th></th>
                <th></th>
               <th></th>
              <th></th>
              <th></th>
              <th>$challan_total</th>
            </tr>
           ";


}
function getBody($interstate, $challan_id){
  include("secure/db_connect.php");
  global $challan_total;
    $string = '';
    $s_no = 0;
  $q = "SELECT i.item_code, i.name, c.unit_price,c.quantity, i.value, g.CGST, g.SGST, g.IGST, g.HSN  FROM tax_rates AS g, challan_item_relation AS c, table_item AS i WHERE g.HSN = i.HSN AND c.challan_id = ? AND i.item_code = c.item_id";
  if($tot_items = $mysqli->prepare( $q ) ){
  $tot_items->bind_param('s', $challan_id);
  if( ! $tot_items->execute()) echo  $tot_items->error;
  $tot_items->store_result();
  $tot_items->bind_result($item_code, $name, $unit_price, $quantity, $value, $CGST, $SGST, $IGST, $HSN );

  } else echo $mysqli->error();
  while($tot_items->fetch() ){
    $s_no ++ ;
    if($interstate) {
      $t =  $quantity * $unit_price * ($SGST + $CGST + 1);
      $challan_total = $challan_total + $t;
       $string.= "
              <tr>
                <td>$s_no</td>
                <td>$HSN</td>
                <td>$item_code</td>
                <td>$name</td>
                <td>$quantity</td>
                <td>Pcs</td>
                <td>".($quantity*$unit_price)."</td>
                <td>".($CGST * 100)." %</td>
                <td>".($SGST * 100)." %</td>
                <td>0%</td>
                <td>".$quantity * $unit_price * $CGST ."</td>
                <td>".$quantity * $unit_price * $SGST ."</td>
                <td>0</td>
                <td>". $t ."</td>
              </tr>
            ";}
    else  {  $t =  $quantity * $unit_price * ($SGST + $CGST + 1);
      $challan_total = $challan_total + $t;
      $string.= "
              <tr>
                <td>$s_no</td>
                  <td>$HSN</td>
                <td>$item_code</td>
                <td>$name</td>
                <td>$quantity</td>
                <td>Pcs</td>
                <td>".($quantity*$unit_price)."</td>
                <td>0%</td>
                <td>0%</td>
                <td>".($IGST * 100)." % </td>
                <td>0</td>
                <td>0</td>
                <td>".  ($quantity * $unit_price * $IGST) ."</td>
                <td>".  $t."</td>
              </tr>
            "; 
    }
  }

  return $string;

}
?>