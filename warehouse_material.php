<?php include ("includes/header.php");?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      
            <link href="dist/css/jquery-ui.min.css" rel="stylesheet">
    
      
      
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Items at Warehouse
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Warehouse</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        <div class="input-group input-group-sm col-sm-4">
                <div class="form-group">
                                            <label>Select Warehouse</label>
                                            <select class="form-control check">
                                                
                                                <option>Select</option>
                                                
                                                <?php 
                                                $warehouse = "SELECT location_id FROM table_location WHERE location_type='warehouse'";
                                                
                                                if($Items  = $mysqli->prepare( $warehouse )){
                                                    $Items->bind_param('s',$challan_id);
                                                    $Items ->execute();
                                                    $Items ->store_result();
                                                    $Items ->bind_result($loc_id);   

                                                    }else echo $mysqli->error;

                                                while( $Items->fetch()){
                                                    
                                                    echo '<option value="'.$loc_id.'">'.$loc_id.'</option>';
                                                }
                                                
                                                ?>
                                                
                                                
                                            </select>
                                        </div>
            </div>
        <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Items</h3>
            </div>
            <!-- /.box-header -->
              <div id="location_details" class="box-body table-responsive no-padding">          
              </div>
              
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
        
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
<script src="dist/js/material_at_loc.js"></script>